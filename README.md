# sisop-praktikum-fp-2023-BJ-U07

## FP Sisop 2023 U07 Formal Report

Group Members:
1. Khairiya Maisa Putri (5025211192)
2. Fauzan Ahmad Faisal (5025211067)
3. Muhammad Fadhlan Ashila Harashta (5025211068)

## A. Autentikasi
from client.c
```c
sprintf(full_command, "\"root\" %s", command);

printf("Full command: %s\n", full_command);
                
if (send(sock, full_command, strlen(full_command), 0) < 0) {
    perror("Gagal mengirim data pengguna ke server");
    exit(EXIT_FAILURE);
}

char server_response[2000];
if (recv(sock, server_response, sizeof(server_response), 0) < 0) {
    perror("Gagal menerima respons dari server");
    exit(EXIT_FAILURE);
    }

printf("Respon dari server: %s\n", server_response);
}
```
from database.c
```c
if(strcmp(detect, "CREATE") == 0){
    if(strcmp(argument, "USER") == 0){
        char user[100];
        char password[100];
        sscanf(cmnd, "CREATE USER %s IDENTIFIED BY %s", user, password);
        saveUser(user, password);
    }

```
## B. Autorisasi
```c
int bytes_received = recv(new_socket, buffer, sizeof(buffer) - 1, 0);
if (bytes_received < 0) {
    perror("Gagal menerima data dari client");
    exit(EXIT_FAILURE);
}
buffer[bytes_received] = '\0'; // Null-terminate the received data

printf("Command: %s\n", buffer);

```
```c
else if(strcmp(detect, "GRANT") == 0){

    char nama_database[100];
    char user_database[100];

    sscanf(cmnd, "GRANT PERMISSION %s INTO %s", nama_database, user_database);
  
    give_permission(nama_database, user_database);
}

```
```c
void give_permission(const char* database, const char* user){

    if(cek_database(database) == 1){

        if(isUserExist(user) == 0){
            puts("Pengguna tidak tersedia");
            return;
        }

        else{
            int hasil = cek_permission(database, user);

            if(hasil == 0) hak_akses(user, database);
            else puts("User telah memiliki akses");
        }
    }
    else{
        puts("Database tidak tersedia");
        return;
    }

}

```
## C. Data Definition Language
Create database
```c
if(strcmp(detect, "CREATE") == 0){
    //jika kata selanjutnya merupakan USER
    if(strcmp(argument, "USER") == 0){

        char user[100];
        char password[100];

        //mengambil value user dan password dari terminal
        sscanf(cmnd, "CREATE USER %s IDENTIFIED BY %s", user, password);
        //menjalankan fungsi saveUser()
        saveUser(user, password);
    }
    //jika kata selanjutnya merupakan DATABASE
    else if(strcmp(argument, "DATABASE") == 0){
        char database_name[100];
        //mengambil value database_name
        sscanf(cmnd, "CREATE DATABASE %s", database_name);
        //menjalankan fungsi createDatabase()
        createDatabase(player, database_name);
    }

```
Function to create database
```c
void createDatabase(char* player, char* database_name) {
    // Tambahkan logika untuk membuat database

    //kita lakukan mkdir karena database merupakan suatu folder
    char command[100], command2[500];
    sprintf(command, "mkdir databases/%s", database_name);

    // Menjalankan perintah menggunakan system()
    int status = system(command);

    if (status == 0) {
        printf("Membuat database '%s'\n", database_name);
        strcpy(server_message, "Database berhasil dibuat");
    } 
    
    else {
        printf("Gagal membuat database '%s'\n", database_name);
        strcpy(server_message, "Gagal membuat database");
    }

    //membuat permission dari suatu database baru dengan user yang sedang dijalankan
    hak_akses(player, database_name);

}
```
## D. Data Manipulation Language
Insert value into database
```c
else if(strcmp(detect, "INSERT") == 0){

        char database[500];
        getDatabaseFromFile(player, "use_database.txt", database);

        printf("database: %s\n", database);

        char tableName[500];
        char val[500];
        if (database[0] != '\0'){

            sscanf(cmnd, "INSERT INTO %s (%[^)])", tableName, val); 
            char filename[10000];
            sprintf(filename, "databases/%s/%s.txt", database, tableName);

            if(isTableExists(filename) == 1){
                
                insert(cmnd, database, tableName);
            }
            else puts("Table tidak tersedia");
        }

        else puts("User belum mengakses database apapun. Gunakan USE untuk akses suatu database");
    }
```
insert function
```c
void insert(const char* cmnd, const char* database, const char* tableName){

    char tmp[1000];
    strcpy(tmp, cmnd);

    char* token = strtok(tmp, "(");
    printf("token1 = %s\n", token);
    token = strtok(NULL, "(");
    printf("token2 = %s\n", token);
    token = strtok(token, ")");
    printf("token3 = %s\n", token);
    token = strtok(token, ",");
    printf("token4 = %s\n", token);

    char data[100][100];

    int i = 0;

    while(token != NULL){
        strcpy(data[i], trim(token));
        i++;
        token = strtok(NULL, ",");
        printf("loop token = %s\n", token);
    }

    FILE *filein, *fileout;

    char open[1000] = {0}, appends[1000] = {0};
    sprintf(open, "databases/%s/struktur_%s.txt", database, tableName);

    printf("open: %s\n", open);
    filein = fopen(open, "r");

    char data_type[100][1000];
    char tmp_type[1000];
    int k = 0, l=0;

    //cek table
    if(filein){
        while(fscanf(filein,"%s",tmp_type) != EOF){
            if (l%2 == 1){
                strcpy(data_type[k],tmp_type);
                printf("Datatype[%d]=%s\n",k,data_type[k]);
                k++;
            }
            l++;
        }
    }
    else{
        puts("Table tidak ada");
        return;
    }

    printf("k = %d, i = %d\n", k, i);

    sprintf(appends, "databases/%s/%s.txt", database, tableName);
    fileout = fopen(appends, "a");

    if(k != i){
        fclose(filein);
        fclose(fileout);

        puts("colom count tidak cocok");
        return;
    }

    for(int j = 0;j < i; j++){
        int val = validasi(data[j]);
        printf("nilai validasi: %d\n", val);
        if(val == 1 && !strcmp(data_type[j],"string")) ;
        else if (val == 2 && !strcmp(data_type[j],"int")) ;
        else {
            fclose(filein);
            fclose(fileout);
            puts("Invalid input");
            return;
        }  
    }

    for(int j = 0;j< i - 1;j++){

        fprintf(fileout,"%s,", data[j]);
        printf("data: %s\n", data[j]);
    }

    fprintf(fileout,"%s\n", data[i-1]);
    fclose(filein);
    fclose(fileout);
}
```
the rest command like update, delete from, select etc exist in database.c
## E. Logging
```c
// Menerima informasi dari client
int bytes_received = recv(new_socket, buffer, sizeof(buffer) - 1, 0);
if (bytes_received < 0) {
    perror("Gagal menerima data dari client");
    exit(EXIT_FAILURE);
}
buffer[bytes_received] = '\0'; // Null-terminate the received data

printf("Command: %s\n", buffer);

//send keterangan command
send(new_socket, buffer, strlen(buffer), 0);

// Mengirim respons ke client
if (send(new_socket, "Pesan diterima oleh server", strlen("Pesan diterima oleh server"), 0) < 0) {
    perror("Gagal mengirim respons ke client");
    exit(EXIT_FAILURE);
}

// Separate the string based on quotation marks
char *player = strtok(buffer, "\"");

printf("player: %s\n", player);

// Get the remaining part of the string after the separation
char *cmnd = strtok(NULL, ";");

// Skip leading spaces in the remaining string
cmnd += strspn(cmnd, " ");

printf("command: %s\n", cmnd);

//menuliskan di log
log_command(player, cmnd);
```
```c
//untuk membuat log dari history perintah
void log_command(const char *username, const char *command) {
    //menggunakan time saat ini
    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm_info);

    FILE *file = fopen("log.txt", "a");
    if (file == NULL) {
        perror("Gagal membuka file log.txt");
        return;
    }

    //menghilangkan ";"
    char cleaned_command[100];
    strcpy(cleaned_command, command);
    char *semicolon_pos = strchr(cleaned_command, ';');
    if (semicolon_pos != NULL) {
        *semicolon_pos = '\0';
    }

    //memasukkan ke log.txt
    fprintf(file, "%s:%s:%s\n", timestamp, username, cleaned_command);

    fclose(file);
}

```
## F. Reliability
Reliability is achieved by creating a separate program from `client.c` to perform database dumping using commands that will be printed on the screen. This program also needs to go through the authentication process first. To perform this step, we create the `dump_client.c` file as follows:
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <arpa/inet.h>

#define SERVER_IP "127.0.0.2"
#define SERVER_PORT 8889

int main(int argc, char* argv[]) {
    int sock = socket(AF_INET, SOCK_STREAM, 0);

    if (sock == -1) {
        perror("Failed to make socket");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(SERVER_PORT);
    if (inet_pton(AF_INET, SERVER_IP, &(server_address.sin_addr)) <= 0) {
        perror("Invlid server address");
        exit(EXIT_FAILURE);
    }

    if (connect(sock, (struct sockaddr*)&server_address, sizeof(server_address)) < 0) {
        perror("Connection to server failed");
        exit(EXIT_FAILURE);
    }
    
    char user[2000], password[2000];
    int option;

    while ((option = getopt(argc, argv, "u:p:")) != -1) {
        switch (option) {
            case 'u':
                strncpy(user, optarg, sizeof(user) - 1);
                break;
            case 'p':
                strncpy(password, optarg, sizeof(password) - 1);
                break;
            default:
                printf("Invalid choice\n");
                exit(EXIT_FAILURE);
        }
    }

    char database[2000];
    if (optind < argc) {
        strncpy(database, argv[optind], sizeof(database) - 1);
    } 
    else {
        printf("Database name not found\n");
        exit(EXIT_FAILURE);
    }

    FILE* file = fopen("users.txt", "r");
    int userFound = 0;
    if (file == NULL) {
        system("touch users.txt");
        system("sudo chmod 777 users.txt");

        FILE* file2 = fopen("users.txt", "r");
        char line[100];
        while (fgets(line, sizeof(line), file2)) {
            line[strcspn(line, "\n")] = '\0';
            if (strstr(line, user) != NULL && strstr(line, password) != NULL) {
                userFound = 1;
                break;
            }
        }
        fclose(file2);
    } 
    else {
        char line[100];
        while (fgets(line, sizeof(line), file)) {
            line[strcspn(line, "\n")] = '\0';
            if (strstr(line, user) != NULL && strstr(line, password) != NULL) {
                userFound = 1;
                break;
            }
        }

        fclose(file);
    }

    if (userFound == 0) {
        printf("User not found\n");
        exit(EXIT_FAILURE);
    } 
    else {
        char cmdUseDB[12002];
        sprintf(cmdUseDB, "\"%s\" USE \"%s\"", user, database);
        if (send(sock, cmdUseDB, strlen(cmdUseDB), 0) < 0) {
            perror("Failed to send user data to server");
            exit(EXIT_FAILURE);
        }

        char full_command[12002];
        sprintf(full_command, "\"%s\" DUMP", user);
        if (send(sock, full_command, strlen(full_command), 0) < 0) {
            perror("Failed to send user data to server");
            exit(EXIT_FAILURE);
        }

        char server_response[2000];
        if (recv(sock, server_response, sizeof(server_response), 0) < 0) {
            perror("Failed to receive a response from the server");
            exit(EXIT_FAILURE);
        }

        char* word = "DROP TABLE";

        while (1) {
            sprintf(full_command, "\"%s\" DUMP", user);
            if (send(sock, full_command, strlen(full_command), 0) < 0) {
                perror("Failed to send user data to server");
                exit(EXIT_FAILURE);
            }

            if (recv(sock, server_response, sizeof(server_response), 0) < 0) {
                perror("Failed to receive a response from the server");
                exit(EXIT_FAILURE);
            }

            printf("%s\n", server_response);

            if (strstr(server_response, word) != NULL) {
                exit(1);
            } else {
                continue;
            }
        }
    }

    close(sock);

    return EXIT_SUCCESS;
}

```

The `client_dump.c` program is basically **similar to client.c**, but it focuses only on performing file dumping. First, the code creates a variable named **sock** to store the socket descriptor. The **socket()** function is used to create a socket using the **AF_INET** parameter, which indicates the use of the IPv4 protocol, **SOCK_STREAM**, which indicates the type of socket used for TCP connections, and 0, which indicates the use of the default protocol for that socket type.

Then, it checks whether the socket creation was successful or not by comparing the value of "sock" with -1. If the value of "sock" is equal to -1, an error occurred in socket creation. The error message "Failed to create socket" will be displayed using the **perror()** function, and the program will exit with a status of **EXIT_FAILURE**.

Next, a **sockaddr_in** structure is created to store the address of the server to be contacted. The server's IP address is stored as a string in the variable **SERVER_IP**, while the server's port is stored in the variable **SERVER_PORT**. The server_address structure is filled with the IP address and port information using the **inet_pton()** function.

After that, a connection to the server is established using the **connect()** function. The first parameter is the socket descriptor "sock", the second parameter is a pointer to the sockaddr structure pointing to server_address, and the third parameter is the size of the server_address structure. If the connection fails, the **connect()** function will return a value less than 0. In this case, the error message "Failed to connect to the server" will be displayed using the **perror()** function, and the program will exit with a status of **EXIT_FAILURE**.
```c
    int sock = socket(AF_INET, SOCK_STREAM, 0);

    if (sock == -1) {
        perror("Failed to make socket");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(SERVER_PORT);
    if (inet_pton(AF_INET, SERVER_IP, &(server_address.sin_addr)) <= 0) {
        perror("Invlid server address");
        exit(EXIT_FAILURE);
    }

    if (connect(sock, (struct sockaddr*)&server_address, sizeof(server_address)) < 0) {
        perror("Connection to server failed");
        exit(EXIT_FAILURE);
    }

```
The next step is to retrieve the command line arguments and process them. This code also reads the "users.txt" file to match the given username and password.

First, the **user** and **password** variables are declared as character arrays with a capacity of 2000. The **option** variable is an integer variable used to store the options given from the command line.

Next, a loop is performed using the `getopt()` function to read the options given from the command line. The **"u"** option is used to retrieve the username argument value, and the **"p"** option is used to retrieve the password argument value. The found argument values are copied to the **user** and **password** variables using the `strncpy()` function. If an invalid option is given, the program will print the message "Invalid option" and exit the program with a status of **EXIT_FAILURE**.

After that, the **database** variable is declared as a character array with a capacity of 2000. The value of the database is retrieved from the command line arguments using the `optind` index. If no argument is given, the program will print the message "Database name not found" and exit the program with a status of **EXIT_FAILURE**.

Next, the "users.txt" file is opened with the "r" (read-only) mode. If the file is not found, the file is created using the `touch` command and given 777 access rights using the `chmod` command. Then, the file is reopened with the "r" mode. In both cases, the **file** variable will hold a pointer to the opened file.

Then, line by line reading is performed from the opened file using the `fgets()` function. Each read line is then stripped of the newline character ('\n') using the `strcspn()` function. Then, matching is performed using the `strstr()` function to find if the line contains matching **username** and **password** values. If a match is found, the **userFound** variable is set to 1, and the loop is stopped using the **break** statement.

Finally, the opened file is closed using the `fclose()` function to prevent resource leaks.
```c
    char user[2000], password[2000];
    int option;

    while ((option = getopt(argc, argv, "u:p:")) != -1) {
        switch (option) {
            case 'u':
                strncpy(user, optarg, sizeof(user) - 1);
                break;
            case 'p':
                strncpy(password, optarg, sizeof(password) - 1);
                break;
            default:
                printf("Invalid choice\n");
                exit(EXIT_FAILURE);
        }
    }

    char database[2000];
    if (optind < argc) {
        strncpy(database, argv[optind], sizeof(database) - 1);
    } 
    else {
        printf("Database name not found\n");
        exit(EXIT_FAILURE);
    }

    FILE* file = fopen("users.txt", "r");
    int userFound = 0;
    if (file == NULL) {
        system("touch users.txt");
        system("sudo chmod 777 users.txt");

        FILE* file2 = fopen("users.txt", "r");
        char line[100];
        while (fgets(line, sizeof(line), file2)) {
            line[strcspn(line, "\n")] = '\0';
            if (strstr(line, user) != NULL && strstr(line, password) != NULL) {
                userFound = 1;
                break;
            }
        }
        fclose(file2);
    } 
    else {
        char line[100];
        while (fgets(line, sizeof(line), file)) {
            line[strcspn(line, "\n")] = '\0';
            if (strstr(line, user) != NULL && strstr(line, password) != NULL) {
                userFound = 1;
                break;
            }
        }

        fclose(file);
    }
```
After searching for a match between the **user** and **password** in the "users.txt" file, there are several actions that need to be taken as follows:

First, a check is performed whether `userFound` is equal to 0. If yes, it means no matching user is found in the "users.txt" file. In this case, the program prints the message "User not found" and exits the program with a status of `EXIT_FAILURE`.

If `userFound` is not 0, it means the user is found in the "users.txt" file. At this stage, the code forms the `cmdUseDB` string using the `sprintf()` function. This string contains the SQL command to change the database to be used. This command is sent to the server using the `send()` function with the `sock` socket.

Next, the code forms the `full_command` string, which contains the SQL command to perform database dumping. This command is also sent to the server using the `send()` function.

After sending the dump command, the code receives the response from the server using the `recv()` function. The server's response is stored in the `server_response` string. If the sending or receiving of data fails, the program prints the corresponding error message and exits the program with a status of `EXIT_FAILURE`.

After receiving the response from the server, the code enters a continuous loop (`while (1)`) to continue sending dump commands and receiving responses from the server. Inside the loop, the `full_command` is sent to the server using the `send() `function, and the response from the server is received using the `recv()` function. The response is then printed using `printf()`.

Next, a check is performed whether the server's response contains the string "DROP TABLE" using the `strstr()` function. If a match is found, the program exits the loop using `exit(1)`, which means exiting the program with a status of `EXIT_FAILURE`. If no match is found, the loop continues.

After exiting the loop, the `sock` socket is closed using the `close()` function. Finally, the program exits with a status of `EXIT_SUCCESS`.
```c
    if (userFound == 0) {
        printf("User not found\n");
        exit(EXIT_FAILURE);
    } 
    else {
        char cmdUseDB[12002];
        sprintf(cmdUseDB, "\"%s\" USE \"%s\"", user, database);
        if (send(sock, cmdUseDB, strlen(cmdUseDB), 0) < 0) {
            perror("Failed to send user data to server");
            exit(EXIT_FAILURE);
        }

        char full_command[12002];
        sprintf(full_command, "\"%s\" DUMP", user);
        if (send(sock, full_command, strlen(full_command), 0) < 0) {
            perror("Failed to send user data to server");
            exit(EXIT_FAILURE);
        }

        char server_response[2000];
        if (recv(sock, server_response, sizeof(server_response), 0) < 0) {
            perror("Failed to receive a response from the server");
            exit(EXIT_FAILURE);
        }

        char* word = "DROP TABLE";

        while (1) {
            sprintf(full_command, "\"%s\" DUMP", user);
            if (send(sock, full_command, strlen(full_command), 0) < 0) {
                perror("Failed to send user data to server");
                exit(EXIT_FAILURE);
            }

            if (recv(sock, server_response, sizeof(server_response), 0) < 0) {
                perror("Failed to receive a response from the server");
                exit(EXIT_FAILURE);
            }

            printf("%s\n", server_response);

            if (strstr(server_response, word) != NULL) {
                exit(1);
            } else {
                continue;
            }
        }
    }

    close(sock);

    return EXIT_SUCCESS;
}
```
The database dump program should be executed every hour for all databases and logs. Afterward, we need to zip the files according to the timestamp, and then empty the logs. Here is the bash file used to execute the requested conditions:

```sh
#!/bin/bash

backup="/home/ubuntu/Documents/fpsisop/database"
runtime="/home/ubuntu/Documents/fpsisop"
timestamp=$(date +\%Y-\%m-\%d_\%H:\%M:\%S)
zip_file="$timestamp.zip"

# Make a backkup file for every child directory
for dir in "$backup"/*; do
  cd /home/ubuntu/Documents/fpsiso/ && \
  ./dump -u taylor -p swift "$(basename "$dir")" > "$backup_dir/$(basename "$dir").backup"
done

# Copy log.txt file to the backup directory
cp "$runtime/log.txt" "$backup_dir"

# Emptying out the real log.txt file
echo -n > "$runtime/log.txt"

# Make zip files from every backup file and log.txt with the timestamp format
cd "$backup" && zip "$zip_file" *.backup log.txt

# Deleting files with the same name of files inside zip
find "$backup" -maxdepth 1 -type f ! -name "$zip_file" -delete

#0 * * * * /home/ubuntu/Documents/fpsiso/cron.sh

```
This script defines several variables, such as `backup` which indicates the directory where backup files will be stored, and `runtime` which indicates the directory where log files and the script are located. Then, the script creates a timestamp that will be used in the backup zip file name.

Next, the script performs backup for each child directory within `backup`. In a `for` loop, the script switches to the `runtime` directory, executes a command called dump, and saves its output to a backup file with the `.backup` extension in the `backup` directory.

After that, the script copies the `log.txt` file from the `runtime` directory to the `backup` directory. Then, the script empties the contents of the original `log.txt` file using the command `echo -n > "$runtime/log.txt".`

Next, the script creates a zip file using the `zip` command in the `backup` directory. The zip file will contain all the backup files with the `.backup` extension and the `log.txt` file. The file name of the zip file will use the previously generated timestamp format.

Once the zip file is created, the script deletes all files that have the same name as the zip file, except the zip file itself. This is done using the `find` command, which searches for files in the `backup` directory and deletes them.

In the final part of the code, there is a comment indicating that this script is executed using a cron job, scheduled every full hour ("0 * * * *"). The cron job runs the `cron.sh` script located in the `directory /home/ubuntu/Documents/fpsisop/`.

## G. Tambahan
## H. Error Handling
## I. Containerization

# Database
## Main
```c
int main() {

    // Rest of your code for the main function (including the while loop)
    char use_database[100] = {0};
    //membuat folder databases
    //membuat file permission.txt
    //membuat file use_database.txt
    system("mkdir -p databases");
    system("touch permission.txt");
    system("sudo chmod 777 permission.txt");

    system("touch use_database.txt");
    system("sudo chmod 777 use_database.txt");

    //membuat socket dengan port 8888
    int server_fd, new_socket[1000], valread;
    struct sockaddr_in address;
    int addrlen = sizeof(address);

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Gagal membuat socket");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(SERVER_PORT);

    if (bind(server_fd, (struct sockaddr*)&address, sizeof(address)) < 0) {
        perror("Gagal melakukan binding");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("Gagal mendengarkan koneksi");
        exit(EXIT_FAILURE);
    }

    //memanggil daemon
    create_daemon();

    //mengetahui berapa client yang sedang akses database secara bersamaan
    int jumlah_client = 0;
    char message[1000];
    while (1) {

        //menerima koneksi server
        if ((new_socket[jumlah_client] = accept(server_fd, (struct sockaddr*)&address, (socklen_t*)&addrlen)) < 0) {
            perror("Gagal menerima koneksi");
            exit(EXIT_FAILURE);
        }

        //membuat thread untuk handle banyaknya client
        pthread_create(&(tid[jumlah_client]), NULL, start, &new_socket[jumlah_client]);

        jumlah_client++;

        printf("Client ke-%d terhubung\n", jumlah_client);
    }

    return 0;
}
```
This program is the main function of a server-side application that handles database management. It begins by creating necessary folders and files for storing databases, permissions, and the currently used database. It then establishes a socket connection on port 8888, allowing clients to connect and interact with the database system. The program creates a daemon process to run continuously in the background. It listens for client connections and spawns a new thread for each client to handle their requests concurrently. The program keeps track of the number of connected clients and prints a message when a client connects. Overall, this program sets up the server, manages client connections, and facilitates the concurrent handling of database operations.
## Start database in daemon
```c
void create_daemon(){

    pid_t pid, sid;        // Variabel untuk menyimpan PID

    pid = fork();     // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
    * (nilai variabel pid < 0) */
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
    * (nilai variabel pid adalah PID dari child process) */
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/home/fadhlan/Documents/fpcoba")) < 0) {
        exit(EXIT_FAILURE);
    }
}

```
This program is the main function of a server-side application that handles database management. It begins by creating necessary folders and files for storing databases, permissions, and the currently used database. It then establishes a socket connection on port 8888, allowing clients to connect and interact with the database system. The program creates a daemon process to run continuously in the background. It listens for client connections and spawns a new thread for each client to handle their requests concurrently. The program keeps track of the number of connected clients and prints a message when a client connects. Overall, this program sets up the server, manages client connections, and facilitates the concurrent handling of database operations.
## Log function
```c
void log_command(const char *username, const char *command) {
    //menggunakan time saat ini
    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm_info);

    FILE *file = fopen("log.txt", "a");
    if (file == NULL) {
        perror("Gagal membuka file log.txt");
        return;
    }

    //menghilangkan ";"
    char cleaned_command[100];
    strcpy(cleaned_command, command);
    char *semicolon_pos = strchr(cleaned_command, ';');
    if (semicolon_pos != NULL) {
        *semicolon_pos = '\0';
    }

    //memasukkan ke log.txt
    fprintf(file, "%s:%s:%s\n", timestamp, username, cleaned_command);

    fclose(file);
}

```
The log_command function is used to record user commands along with a timestamp and username in a log file called "log.txt". It retrieves the current time, formats it into a readable string, and opens the log file in append mode. The function then sanitizes the command by removing semicolons, if present, to prevent potential security issues. Finally, it writes the formatted log entry to the file and closes it.
## Handle dump
```c
void handleDump(char *db_name, char *dumpOutput) {
    //cek apakah ada database
    if (strlen(db_name) == 0) {
        puts("No database used");
        return;
    }

    //buat file path
    char filePath[100];
    snprintf(filePath, sizeof(filePath), "databases/%s", db_name);

    DIR *dir = opendir(filePath);
    if (dir == NULL) {
        perror("Error opening directory");
        printf("Directory path: %s\n", filePath);
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG && strncmp(entry->d_name, "struktur_", 9) != 0) {
            // Create the file path
            char file[2000];
            snprintf(file, sizeof(file), "%s/%s", filePath, entry->d_name);

            // Open the file
            FILE *filePtr = fopen(file, "r");
            if (filePtr == NULL) {
                printf("Error opening file: %s\n", file);
                continue;
            }

            // Read the first line
            char firstLine[1000];
            if (fgets(firstLine, sizeof(firstLine), filePtr) != NULL) {
                // Remove newline character from the end of the line
                if (firstLine[strlen(firstLine) - 1] == '\n') {
                    firstLine[strlen(firstLine) - 1] = '\0';
                }

                // Remove file extension
                char *extension = strrchr(entry->d_name, '.');
                if (extension != NULL) {
                    *extension = '\0';
                }

                // Append the output to the dumpOutput variable
                sprintf(dumpOutput + strlen(dumpOutput), "DROP TABLE %s;\n", entry->d_name);
                sprintf(dumpOutput + strlen(dumpOutput), "CREATE TABLE %s (%s);\n", entry->d_name, firstLine);

                // Read the rest of the lines
                char line[1000];
                while (fgets(line, sizeof(line), filePtr) != NULL) {
                    // Remove newline character from the end of the line
                    if (line[strlen(line) - 1] == '\n') {
                        line[strlen(line) - 1] = '\0';
                    }

                    // Append the output to the dumpOutput variable
                    sprintf(dumpOutput + strlen(dumpOutput), "INSERT INTO %s (%s);\n", entry->d_name, line);
                }

                sprintf(dumpOutput + strlen(dumpOutput), "\n");
            }

            // Close the file
            fclose(filePtr);
        }
    }

    closedir(dir);
}

``` 
The handleDump function creates a database dump by extracting table structure and data from a specified database directory. It iterates through each file in the directory, reads the file contents, and generates SQL statements for table creation and data insertion. The resulting SQL statements are appended to the dumpOutput variable.
## Select table
```c
if (!strlen(database)) {
        puts("no database used");
        return;
    }

    printf("database: %s\n", database);

    char temp[1024] = {0};
    strcpy(temp, cmnd);

    char* token = strtok(temp, ";");
    token = strtok(temp, " ");
    char input[10][1000];
    int i = 0;

    // Menyimpan potongan command kedalam array input 
    while (token != NULL) {
        strcpy(input[i++], token);
        token = strtok(NULL, " ");
    }

    printf("Input values:\n");
    for (int j = 0; j < i; j++) {
        printf("input[%d]: %s\n", j, input[j]);
    }

    int all, w = -1, k, kasus, urut[20];
    char banding[1000], temporary[1000], cmpwhere[1000], ret[1000];
    char path[10000], struk[10000], n_path[10000], cmp[20][1000];

    FILE *strukturin;

    // Untuk mendapatkan select "*"
    if (!strcmp(input[1], "*")) {
        sprintf(struk, "databases/%s/struktur_%s.txt", database, input[3]);

        strukturin = fopen(struk, "r");

        if (!strukturin) {
            puts("table does not exist");
            return;
        }

        fclose(strukturin);

        all = -1;
        kasus = 2;
        if (strcmp(input[2], "FROM")) {
            puts("invalid syntax");
            return;
        }
        // Cek klausa where
        if (i >= 5 && !strcmp(input[4], "WHERE")) {
            token = strtok(input[5], "=");
            strcpy(banding, token);
            token = strtok(NULL, "=");
            strcpy(cmpwhere, token);

            k = 0;

            if (!strcmp(input[4], "WHERE")) {
                strukturin = fopen(struk, "r");
                while (fscanf(strukturin, "%s %s", temporary, ret) != EOF) {
                    if (!strcmp(banding, temporary)) {
                        w = k;
                    }
                    k++;
                }
                if (w == -1) {
                    fclose(strukturin);
                    puts("column does not exist");
                    return;
                }
                fclose(strukturin);
            }
        }
    }
    // Untuk mendapatkan select beberapa parameter kolom
    else {
        kasus = 2;
        while (kasus <= i && strcmp(input[kasus++], "FROM")) {}

        if (kasus > i) {
            puts("invalid syntax");
            return;
        }
        sprintf(struk, "databases/%s/struktur_%s.txt", database, input[kasus]);
        strukturin = fopen(struk, "r");

        if (!strukturin) {
            puts("table does not exist");
            return;
        }

        fclose(strukturin);

        all = 0;
        kasus = 1;

        for (int oo = 0; oo < 20; oo++) {
            urut[oo] = -1;
        }

        char temp2[1000];
        while (kasus < i && strcmp(input[kasus], "FROM")) {
            k = 0;
            strukturin = fopen(struk, "r");
            while (fscanf(strukturin, "%s %s", temporary, ret) != EOF) {
                strcpy(temp2, temporary);
                strcat(temp2, ",");
                if (!strcmp(input[kasus], temporary) || !strcmp(temp2, input[kasus])) {
                    strcpy(cmp[all], input[kasus]);
                    urut[all] = k;
                    all++;
                }
                k++;
            }
            if (all == 0 || urut[all - 1] == -1) {
                fclose(strukturin);
                return;
            }
            fclose(strukturin);
            kasus++;
        }
        if (kasus == i) {
            return;
        }
        if (i >= kasus + 2 && !strcmp(input[kasus + 2], "WHERE")) {
            token = strtok(input[kasus + 3], "=");
            strcpy(banding, token);
            token = strtok(NULL, "=");
            strcpy(cmpwhere, token);

            k = 0;

            if (!strcmp(input[kasus + 2], "WHERE")) {
                strukturin = fopen(struk, "r");
                while (fscanf(strukturin, "%s %s", temporary, ret) != EOF) {
                    if (!strcmp(banding, temporary)) {
                        w = k;
                    }
                    k++;
                }
                if (w == -1) {
                    fclose(strukturin);
                    return;
                }
                fclose(strukturin);
            }
        }
    }

    sprintf(path, "databases/%s/%s.txt", database, input[kasus + 1]);
    sprintf(n_path, "Hasil_select.txt");

    printf("path: %s\n", path);
    printf("n_path: %s\n", n_path);

    // Attempt to create the output file using touch
    FILE *touch_file = fopen(n_path, "a");
    if (touch_file) {
        fclose(touch_file);
        puts("Output file created.");
    }

    FILE *filein, *fileout;
    filein = fopen(path, "r");
    
    // Menulis hasil select di hasil_select.txt
    if (filein) {

        // Check if output file exists
        fileout = fopen(n_path, "r");
        if (fileout) {
            // Output file already exists
            fclose(fileout);
            puts("Output file already exists.");
        } else {
            // Output file doesn't exist, create it
            fileout = fopen(n_path, "w");
            if (fileout) {
                fclose(fileout);
                puts("Output file created.");
            } else {
                puts("Failed to create output file.");
                return;
            }
        }
        // Open the output file for writing
        fileout = fopen(n_path, "w");
        if (!fileout) {
            puts("Failed to open output file for writing.");
            return;
        }

        char ambil[1000], lama[1000], baru[1000];

        while (fgets(ambil, 1000, filein)) {
            if (all == -1) {
                if (w == -1) {
                    fprintf(fileout, "%s", ambil);
                }
                else {
                    char hade[1000];
                    strcpy(hade, ambil);
                    token = strtok(ambil, ",");
                    int j = 0;
                    strcpy(baru, "");
                    int flag = 0;
                    while (token != NULL) {
                        if (j == w) {
                            if (!strcmp(cmpwhere, token)) {
                                fprintf(fileout, "%s", hade);
                                break;
                            }
                        }
                        token = strtok(NULL, ",");
                        j++;
                    }
                }
            }
            else {
                char jadi[1000], hade[1000];
                strcpy(jadi, "");
                for (int oo = 0; oo < all; oo++) {
                    strcpy(hade, ambil);
                    token = strtok(hade, ",");
                    int j = 0;
                    strcpy(baru, "");
                    int flag = 0;
                    while (token != NULL) {
                        if (j == urut[oo]) {
                            strcat(jadi, token);
                            strcat(jadi, ",");
                            break;
                        }
                        token = strtok(NULL, ",");
                        j++;
                    }
                }
                jadi[strlen(jadi) - 1] = '\0';
                fprintf(fileout, "%s\n", jadi);
            }
        }

        fclose(filein);
        fclose(fileout);
        puts("Select result has been written to the file 'database/Hasil_select.txt'");
    }
    else {
        puts("Failed to open input or output file");
    }
} 

```
The select_table function takes care of executing a SELECT query on a specific table in the database. It parses the command string and extracts relevant information such as database name, table name, column to select, and WHERE clause (if any). It then reads the table file, applies the selection criteria, and writes the resulting rows to the output file. If the output file already exists, it displays a message indicating its status. Lastly, it informs the user about the output file location.
## Delete from
```c
void delete_from(char *cmnd, char *database) {
    if(!strlen(database)){
        puts("No database used");
        return;
    }

    char temp[1024] = {0};
    strcpy(temp, cmnd);

    // Tokenisasi input untuk mendapat nama tabel
    char* token = strtok(temp, ";");
    token = strtok(temp, " ");
    char input[10][1000];
    int i = 0;

    while (token != NULL) {
        strcpy(input[i++], token);
        token = strtok(NULL, " ");
    }

    // Delete semua baris dari tabel kecuali baris 1 header
    if (i == 3) {
        char path[10000];
        sprintf(path, "databases/%s/%s.txt", database, input[2]);
        FILE *filein, *fileout;
        filein = fopen(path, "r");
        if (filein) {
            char ambil[1000];
            fgets(ambil, 1000, filein);
            fclose(filein);

            fileout = fopen(path, "w");
            fprintf(fileout, "%s\n", ambil);
            fclose(fileout);
        }
        else {
            puts("Table doesn't exist");
            return;
        }
        return;
    }

    // Kasus jika terdapat klausa where
    else if (i == 5) {
        if (strcmp(input[3], "WHERE")) {
            puts("Invalid Syntax");
            return;
        }

        char TableName[1000], val[1000];

        // Tokenisasi untuk mendapat value where
        token = strtok(input[4], "=");
        strcpy(TableName, token);
        token = strtok(NULL, "=");
        strcpy(val, token);

        char path[10000], RefFile[10000], n_path[10000];

        sprintf(RefFile, "databases/%s/struktur_%s.txt", database, input[2]);
        FILE *strukturin;
        strukturin = fopen(RefFile, "r");

        int columnIndex = -1;
        int k = 0;
        char ret[1000], table[1000];
        if (strukturin) {
            while (fscanf(strukturin, "%s %s", table, ret) != EOF) {
                if (!strcmp(TableName, table)) {
                    columnIndex = k;
                }
                k++;
            }
            if (columnIndex == -1) {
                fclose(strukturin);
                puts("Column doesn't exist");
                return;
            }
            fclose(strukturin);
        }
        else {
            puts("Table doesn't exist");
            return;
        }

        sprintf(path, "databases/%s/%s.txt", database, input[2]);
        sprintf(n_path, "databases/%s/%s2.txt", database, input[2]);
        FILE *filein, *fileout;
        filein = fopen(path, "r");
        fileout = fopen(n_path, "w");
        char ambil[1000];
        
        // Mengganti struktur tabel dengan memindahkan file lama yang sesuai ke file baru kemudian direname
        if (filein) {
            int lineCount = 0;
            while (fgets(ambil, 1000, filein)) {
                lineCount++;
                if (lineCount == 1) {
                    fprintf(fileout, "%s", ambil);  // Menyalin baris pertama ke fileout
                    continue;
                }
                token = strtok(ambil, ",");
                int j = 0;
                char baru[1000];
                strcpy(baru, "");
                int flag = 0;
                while (token != NULL) {
                    strcat(baru, token);
                    strcat(baru, ",");
                    if (j == columnIndex) {
                        if (!strcmp(val, token)) {
                            flag = 1;
                        }
                    }
                    token = strtok(NULL, ",");
                    j++;
                }
                if (!flag) {
                    baru[strlen(baru)-1] = 0;
                    fprintf(fileout, "%s", baru);
                    if (baru[strlen(baru)-1] != '\n') {
                        fprintf(fileout, "\n");
                    }
                }
            }
            fclose(filein);
            fclose(fileout);
            remove(path);
            rename(n_path, path);
            return;
        }
    }
}

```
The delete_from function takes care of executing a DELETE FROM query on a specific table in the database. It parses the command string and extracts relevant information such as database name, table name and WHERE clause (if any). It then reads the table file, applies the delete criteria, and writes the updated rows to the new file. Lastly, it replaces the original table file with the updated file. If any error occurs during the process, a corresponding error message will be displayed.
## Update
```c

void update(char *cmnd, char *database){

    if(!strlen(database)){
        puts("No database used");
        return;
    }

    char temp[1024] = {0};
    strcpy(temp,cmnd);

    char* token = strtok(temp, ";");
    token = strtok(temp," ");
    char input[10][1000];
    int i=0;

    // Tokenisasi kemudian disimpan dalam array input
    while (token != NULL) {
        strcpy(input[i++],token);
        token = strtok(NULL, " ");
    }

    if(strcmp(input[2],"SET")){
        puts("Invalid Syntax");
        return;
    }

    char ColName[1000], val[1000];

    token = strtok(input[3],"=");
    strcpy(ColName,token);
    token = strtok(NULL,"=");
    strcpy(val,token);

    char path[10000],RefFile[10000],n_path[10000];

    sprintf(RefFile,"databases/%s/struktur_%s.txt",database,input[1]);
    FILE *strukturin;
    strukturin = fopen(RefFile,"r");

    i = -1;
    int k = 0;
    char ret[1000],table[1000];

    // Cek Tabel
    if(strukturin){
        while(fscanf(strukturin,"%s %s",table,ret) != EOF){
            if(!strcmp(ColName,table)){
                i = k;
            }
            k++;
        }
        if(i == -1){
            fclose(strukturin);
            puts("Column doesn't exist");
        }
        fclose(strukturin);
    }
    else{
        puts("Table doesn't exist");
    }

    int w = -1;
    char req[1000],temporary[1000],valwhere[1000];

    // Kasus ada klausa where
    if(strlen(input[4])){
        token = strtok(input[5],"=");
        strcpy(req,token);
        token = strtok(NULL,"=");
        strcpy(valwhere,token);
        k=0;
        if(!strcmp(input[4],"WHERE")){
            strukturin = fopen(RefFile,"r");
            while(fscanf(strukturin,"%s %s",temporary,ret) != EOF){
                if(!strcmp(req,temporary)){
                    w = k;
                }
                k++;
            }
            if(w == -1){
                fclose(strukturin);
                puts("Column doesn't exist");
                return;
            }
            fclose(strukturin);
        }
    }

    sprintf(path,"databases/%s/%s.txt",database,input[1]);
    sprintf(n_path,"databases/%s/%s2.txt",database,input[1]);
    FILE *filein,*fileout;
    filein = fopen(path,"r");
    fileout = fopen(n_path,"w");
    char ambil[1000], lama[1000];
    
    // Menulis hasil update
    if(filein){
        
        // Membaca dan mengabaikan update baris pertama (header)
        fprintf(fileout, "%s", fgets(ambil, 1000, filein));
        while(fgets(ambil,1000,filein)){
            strcpy(lama,ambil);
            token = strtok(ambil,",");
            int j = 0;
            char baru[1000];
            strcpy(baru,"");
            int flag = 0;
            while(token!=NULL){
                if(j == w){
                    if(!strcmp(valwhere,token)){
                        flag = 1;
                    }
                }
                if(j==i){
                    strcat(baru,val);
                    strcat(baru,",");
                }
                else{
                    strcat(baru,token);
                    strcat(baru,",");
                }
                token = strtok(NULL,",");
                j++;
            }
            if(flag == 1 || w == -1){
                baru[strlen(baru)-1] = 0;
                fprintf(fileout,"%s",baru);
                if(baru[strlen(baru)-1] != '\n'){
                    fprintf(fileout,"\n");
                }
            }
            else{
                lama[strlen(baru)-1] = 0;
                fprintf(fileout,"%s",lama);
                if(lama[strlen(lama)-1] != '\n'){
                    fprintf(fileout,"\n");
                }
            }
        }
        fclose(filein);
        fclose(fileout);
        remove(path);
        rename(n_path,path);
        return;
    }
}

```
The update function is responsible for executing an UPDATE query on a specific table in the database. It parses the command string to extract relevant information such as database name, table name, column name and new values to update. It also takes care of the existence of the WHERE clause and extracts the information needed to filter the rows to be updated. The function reads a table file, applies update criteria, and writes the updated rows to a new file. If an error occurs, the corresponding error message will be displayed. Finally, the original table file is replaced with the updated file, completing the update process.
## Insert
```c
void insert(const char* cmnd, const char* database, const char* tableName){

    char tmp[1000];
    strcpy(tmp, cmnd);

    //mengambil value didalam tanda kurung
    char* token = strtok(tmp, "(");
    printf("token1 = %s\n", token);
    token = strtok(NULL, "(");
    printf("token2 = %s\n", token);
    token = strtok(token, ")");
    printf("token3 = %s\n", token);
    token = strtok(token, ",");
    printf("token4 = %s\n", token);

    char data[100][100];

    int i = 0;

    //mengambil kata dalam koma 
    while(token != NULL){
        strcpy(data[i], trim(token));
        i++;
        token = strtok(NULL, ",");
        printf("loop token = %s\n", token);
    }

    FILE *filein, *fileout;

    char open[1000] = {0}, appends[1000] = {0};
    sprintf(open, "databases/%s/struktur_%s.txt", database, tableName);

    printf("open: %s\n", open);
    filein = fopen(open, "r");

    char data_type[100][1000];
    char tmp_type[1000];
    int k = 0, l=0;

    //cek table
    if(filein){
        while(fscanf(filein,"%s",tmp_type) != EOF){
            if (l%2 == 1){
                strcpy(data_type[k],tmp_type);
                printf("Datatype[%d]=%s\n",k,data_type[k]);
                k++;
            }
            l++;
        }
    }
    else{
        puts("Table tidak ada");
        return;
    }

    printf("k = %d, i = %d\n", k, i);

    sprintf(appends, "databases/%s/%s.txt", database, tableName);
    fileout = fopen(appends, "a");

    if(k != i){
        fclose(filein);
        fclose(fileout);

        puts("colom count tidak cocok");
        return;
    }

    //melakukan validasi apakah tipe data sesuai dengan yang diinginkan
    for(int j = 0;j < i; j++){
        int val = validasi(data[j]);
        printf("nilai validasi: %d\n", val);
        if(val == 1 && !strcmp(data_type[j],"string")) ;
        else if (val == 2 && !strcmp(data_type[j],"int")) ;
        else {
            fclose(filein);
            fclose(fileout);
            puts("Invalid input");
            return;
        }  
    }

    for(int j = 0;j< i - 1;j++){

        fprintf(fileout,"%s,", data[j]);
        printf("data: %s\n", data[j]);
    }

    fprintf(fileout,"%s\n", data[i-1]);
    fclose(filein);
    fclose(fileout);
}

```
The insert function is responsible for executing the INSERT query to add a new data row to a specified table in the database. The first function extracts the values in brackets from the command string. It then parses these values, truncates them, and stores them in an array. The function reads the table structure file to determine the expected data type for each column. If the sum of the values supplied does not match the sum of the columns, an error will be returned. The function then performs validation on each value to ensure it matches the expected data type. If the value is found to be invalid, an error is returned. Finally, the function opens the table file in "append" mode and writes the value as a new line. The file is then closed, completing the insert process.
## Get database
```c
void getDatabaseFromFile(const char* user, const char* filename, char* database) {
    FILE* file = fopen(filename, "r");

    if (file == NULL) {
        printf("Failed to open the file\n");
        return;
    }

    char line[500];
    while (fgets(line, sizeof(line), file)) {
        char tmpUser[100], tmpDatabase[100];
        if (sscanf(line, "%s %s", tmpUser, tmpDatabase) == 2) {
            if (strcmp(tmpUser, user) == 0) {
                strcpy(database, tmpDatabase);
                fclose(file);
                return;
            }
        }
    }

    printf("User not found in the file\n");
    fclose(file);
}

```
The getDatabaseFromFile function reads the specified file which contains the user database mappings and retrieves the database associated with the specified user name. It loops through each line of the file, extracting the corresponding username and database name. If a match is found for the given username, the associated database name is copied to the output variable. If no matches are found, an error message is displayed. In either case, the file is closed before the function returns. 
## Save user
```c
void saveUser(const char* user, const char* password) {
    if (isUserExist(user)) {
        printf("Pengguna '%s' sudah ada\n", user);
        strcpy(server_message, "User telah tersedia");
        return;
    }
    else strcpy(server_message, "User berhasil disimpan di database");

    FILE* file = fopen("users.txt", "a");
    if (file == NULL) {
        perror("Gagal membuka file users.txt");
        return;
    }

    fprintf(file, "%s %s\n", user, password);
    fclose(file);

    printf("Menyimpan user '%s' dengan password '%s' ke dalam database\n", user, password);
}


```
The saveUser function is responsible for saving the corresponding user and password to a file named "users.txt". First, it checks whether the user exists by calling the isUserExist function. If the user already exists, an error message is printed and the function is returned. Otherwise, the success message is stored in the server_message variable.

Next, the function opens the "users.txt" file in add mode. If the file fails to open, an error message is displayed, and the function returns. If the file opens successfully, the user and password will be written to a new line in the file using the fprintf function. Finally, the file is closed, and a message confirming the successful saving of the user and password is printed to the console.
## Give permission
```c
void give_permission(const char* database, const char* user){

    //cek apakah database telah terbuat
    if(cek_database(database) == 1){
        //cek apakah user tersebut ada
        if(isUserExist(user) == 0){
            puts("Pengguna tidak tersedia");
            return;
        }

        else{
            int hasil = cek_permission(database, user);
            //cek apakah user telah memiliki akses database tersebut atau belum
            if(hasil == 0) hak_akses(user, database);
            else puts("User telah memiliki akses");
        }
    }
    else{
        puts("Database tidak tersedia");
        return;
    }

}

```
The give_permission function is responsible for granting permissions to a user for a particular database. It checks for the existence of both the database and the user, and if both exist, it verifies whether the user already has permissions for the database. Otherwise, the user is granted the permission by calling the access_rights function. The function ensures proper permission management by handling error cases and existing permissions, ensuring that users are only granted permissions when needed.
### Check login
```c
void cek_login(const char* user) {
    // Buka file use_database.txt untuk membaca
    FILE* file = fopen("use_database.txt", "r");
    if (file == NULL) {
        printf("Gagal membuka file use_database.txt\n");
        return;
    }

    char line[100];
    char tempFile[] = "temp.txt";

    // Buka file temporary untuk menulis
    FILE* temp = fopen(tempFile, "w");
    if (temp == NULL) {
        printf("Gagal membuat file temporary\n");
        fclose(file);
        return;
    }

    int deleted = 0; // Flag untuk menandakan apakah ada baris yang dihapus

    // Baca baris per baris dari file use_database.txt
    while (fgets(line, sizeof(line), file) != NULL) {
        // Periksa apakah baris mengandung substring <user>
        char* token = strstr(line, user);
        if (token != NULL) {
            // Hapus substring <user> dari baris
            memmove(token, token + strlen(user), strlen(token) - strlen(user) + 1);
            deleted = 1;
        } else {
            // Tulis baris yang tidak berubah ke file temporary
            fputs(line, temp);
        }
    }

    // Tutup file
    fclose(file);
    fclose(temp);

    // Hapus file use_database.txt
    if (remove("use_database.txt") == 0) {
        // Ganti nama file temporary menjadi use_database.txt
        if (rename(tempFile, "use_database.txt") != 0) {
            printf("Gagal mengganti nama file\n");
        }
    } else {
        printf("Gagal menghapus file use_database.txt\n");
    }

    if (deleted) {
        printf("Baris dengan user %s dihapus dari file use_database.txt\n", user);
    } else {
        printf("Tidak ada baris dengan user %s dalam file use_database.txt\n", user);
    }
}

```
The check_login function is responsible for checking and managing user login information stored in the "use_database.txt" file. It opens files for reading and creates temporary files for writing. It reads every line from the file and looks for the existence of the specified user. If found, it removes the user substring from the line. Otherwise it writes the unchanged lines to the temporary file. After processing all the lines, it closes both files and starts deleting the original "use_database.txt" file. If the deletion is successful, the temporary file name will be changed to "use_database.txt". The function also provides feedback to the user, indicating whether the user's entry was removed from the file or if no matching entries were found. This code shows the basic mechanism for managing user login data in a file.