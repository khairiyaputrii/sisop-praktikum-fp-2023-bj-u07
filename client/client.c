#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <arpa/inet.h>

#define SERVER_IP "127.0.0.1"
#define SERVER_PORT 8888
#define MAX_COMMAND_LENGTH 2000

int main(int argc, char* argv[]) {
    //membuat socket
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1) {
        perror("Gagal membuat socket");
        exit(EXIT_FAILURE);
    }

    //set up socket ke port 8888
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(SERVER_PORT);
    if (inet_pton(AF_INET, SERVER_IP, &(server_address.sin_addr)) <= 0) {
        perror("Alamat server tidak valid");
        exit(EXIT_FAILURE);
    }

    //connect ke socket
    if (connect(sock, (struct sockaddr*)&server_address, sizeof(server_address)) < 0) {
        perror("Koneksi ke server gagal");
        exit(EXIT_FAILURE);
    }

    //mendapatkan id user
    uid_t id = getuid();

    char command[10000];
    char full_command[12002];

    //user root
    if (id == 0) {
        //dapat melakukan create user dan grant permission
        puts("Kamu bisa melakukan:");
        puts("1. CREATE USER [nama user] IDENTIFIED BY [password];");
        puts("2. GRANT PERMISSION [nama_database] INTO [nama_user];");
        puts("3. USE [nama_database];");
        puts("4. CREATE DATABASE [nama_database];");
        puts("5. CREATE TABLE [nama_tabel] ([nama_kolom] [tipe_data], ...);");
        puts("6. DROP DATABASE [nama_database];");
        puts("7. DROP TABLE [nama_table]");
        puts("8. DROP COLUMN [nama-kolom] FROM [Nama_tabel]");
        puts("9. INSERT INTO [nama_tabel] ([value], ...);");
        puts("10. UPDATE [nama_tabel] SET [kolom]=[nilai];");
        puts("11. UPDATE [nama_tabel] SET [kolom]=[nilai] WHERE [kolom]=[value];");
        puts("12. DELETE FROM [nama_tabel];");
        puts("13. DELETE FROM [nama_tabel] WHERE [kolom]=[value];");
        puts("14. SELECT * FROM [nama_tabel];");
        puts("15. SELECT [Nama_kolom] FROM [nama_tabel];");
        puts("16. SELECT * FROM [nama_tabel] WHERE [kolom]=[value];");
        puts("17. SELECT [nama_kolom] FROM [nama_tabel] WHERE [kolom]=[value];");
        puts("18. EXIT");

        puts("Masukkan command yang kamu inginkan:");

        while (1) {
            //mendapatkan kalimat dari terminal
            fgets(command, sizeof(command), stdin);
            
            //cek apakah isi dari kalimat tersebut mengandung kata "EXIT"
            if(strstr(command, "EXIT") != NULL){
                //menambahkan keterangan ke server bahwa yang masuk adalah user root
                //contoh perintah: "root" EXIT
                sprintf(full_command, "\"root\" %s", command);
                printf("Full command: %s\n", full_command);

                //mengirim perintah melalui socket ke server
                if (send(sock, full_command, strlen(full_command), 0) < 0) {
                    perror("Gagal mengirim data pengguna ke server");
                    exit(EXIT_FAILURE);
                }

                return 0;
            }
            else{

                //memasukkan command selain "EXIT" dan menambahkan keterangan user "root"
                sprintf(full_command, "\"root\" %s", command);

                printf("Full command: %s\n", full_command);
                
                //mengirim perintah melalui socket
                if (send(sock, full_command, strlen(full_command), 0) < 0) {
                    perror("Gagal mengirim data pengguna ke server");
                    exit(EXIT_FAILURE);
                }

                //menerima respon dari server
                char server_response[2000];
                if (recv(sock, server_response, sizeof(server_response), 0) < 0) {
                    perror("Gagal menerima respons dari server");
                    exit(EXIT_FAILURE);
                }

                printf("Respon dari server: %s\n", server_response);
            }
        }
    } 
    
    //user bukan merupakan root
    else {

        char user[2000], password[2000];
        int option;

        //menerima inputan dari user apa dan passwordnya apa
        while ((option = getopt(argc, argv, "u:p:")) != -1) {
            switch (option) {
                //mengambil user
                case 'u':
                    strncpy(user, optarg, sizeof(user) - 1);
                    break;
                //mengambil password
                case 'p':
                    strncpy(password, optarg, sizeof(password) - 1);
                    break;
                default:
                    printf("Pilihan tidak valid\n");
                    exit(EXIT_FAILURE);
            }
        }

        printf("user: %s, password: %s\n", user, password);

        //membuka users.txt yang berisi user yang telah dibuat oleh root
        FILE* file = fopen("users.txt", "r");
        int userFound = 0;
        //jika users.txt belum terbuat maka buat terlebih dahulu
        if (file == NULL) {

            system("touch users.txt");
            system("sudo chmod 777 users.txt");
            
            FILE* file2 = fopen("users.txt", "r");
            char line[100];
            //cek apakah user dan password valid
            while (fgets(line, sizeof(line), file2)) {
                line[strcspn(line, "\n")] = '\0';
                if (strstr(line, user) != NULL) {
                    if (strstr(line, password) != NULL) {
                        userFound = 1;
                        break;
                    }
                }
            }
            fclose(file2);
        }
        else{
            char line[100];
            while (fgets(line, sizeof(line), file)) {
                line[strcspn(line, "\n")] = '\0';
                if (strstr(line, user) != NULL) {
                    if (strstr(line, password) != NULL) {
                        userFound = 1;
                        break;
                    }
                }
            }

            fclose(file);
        }

        if (userFound == 0) {
            printf("User tidak ditemukan\n");
            exit(EXIT_FAILURE);
        } else {
            puts("User ditemukan");
        }

        puts("Kamu bisa melakukan:");
        puts("1. USE [nama_database];");
        puts("2. CREATE DATABASE [nama_database];");
        puts("3. CREATE TABLE [nama_tabel] ([nama_kolom] [tipe_data], ...);");
        puts("4. DROP DATABASE [nama_database];");
        puts("5. DROP TABLE [nama_table]");
        puts("6. DROP COLUMN [nama-kolom] FROM [Nama_tabel]");
        puts("7. INSERT INTO [nama_tabel] ([value], ...);");
        puts("8. UPDATE [nama_tabel] SET [kolom]=[nilai];");
        puts("9. UPDATE [nama_tabel] SET [kolom]=[nilai] WHERE [kolom]=[value];");
        puts("10. DELETE FROM [nama_tabel];");
        puts("11. DELETE FROM [nama_tabel] WHERE [kolom]=[value];");
        puts("12. SELECT * FROM [nama_tabel];");
        puts("13. SELECT [Nama_kolom] FROM [nama_tabel];");
        puts("14. SELECT * FROM [nama_tabel] WHERE [kolom]=[value];");
        puts("15. SELECT [nama_kolom] FROM [nama_tabel] WHERE [kolom]=[value];");
        puts("16. EXIT");


        puts("Masukkan command yang kamu inginkan:");

        while (1) {
            fgets(command, sizeof(command), stdin);

            //cek apakah user selain root mengirimkan perintah "CREATE USER"
            if (strstr(command, "CREATE USER") != NULL) {
                printf("Anda hanya dapat mengirim perintah CREATE USER sebagai root.\n");
                continue;
            } 
            
            //cek apakah user selain root mengirimkan perintah "GRANT PERMISSION"
            else if(strstr(command, "GRANT PERMISSION") != NULL){
                puts("Anda hanya dapat mengirim perintah GRANT PERMISSION sebagai root.");
                continue;
            }

            //cek apakah user selain root mengirimkan perintah "EXIT"
            else if(strstr(command, "EXIT") != NULL){

                sprintf(full_command, "\"%s\" %s", user, command);
                printf("Full command: %s\n", full_command);
                if (send(sock, full_command, strlen(full_command), 0) < 0) {
                    perror("Gagal mengirim data pengguna ke server");
                    exit(EXIT_FAILURE);
                }

                return 0;
            }
            
            //menghandle perintah user selain root
            else {

                //menambahkan user + command
                //contoh: "jisoo" CREATE DATABASE db1;
                sprintf(full_command, "\"%s\" %s", user, command);
                printf("Full command: %s\n", full_command);
                if (send(sock, full_command, strlen(full_command), 0) < 0) {
                    perror("Gagal mengirim data pengguna ke server");
                    exit(EXIT_FAILURE);
                }

                //menerima respon dari server
                char server_response[2000];
                if (recv(sock, server_response, sizeof(server_response), 0) < 0) {
                    perror("Gagal menerima respons dari server");
                    exit(EXIT_FAILURE);
                }

                printf("Respon dari server: %s\n", server_response);
                continue;  // Membaca input lagi
            }
        }
    }

    close(sock);

    return EXIT_SUCCESS;
}

