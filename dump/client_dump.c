#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <arpa/inet.h>

#define SERVER_IP "127.0.0.2"
#define SERVER_PORT 8889

int main(int argc, char* argv[]) {
    int sock = socket(AF_INET, SOCK_STREAM, 0);

    if (sock == -1) {
        perror("Failed to make socket");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(SERVER_PORT);
    if (inet_pton(AF_INET, SERVER_IP, &(server_address.sin_addr)) <= 0) {
        perror("Invlid server address");
        exit(EXIT_FAILURE);
    }

    if (connect(sock, (struct sockaddr*)&server_address, sizeof(server_address)) < 0) {
        perror("Connection to server failed");
        exit(EXIT_FAILURE);
    }
    
    char user[2000], password[2000];
    int option;

    while ((option = getopt(argc, argv, "u:p:")) != -1) {
        switch (option) {
            case 'u':
                strncpy(user, optarg, sizeof(user) - 1);
                break;
            case 'p':
                strncpy(password, optarg, sizeof(password) - 1);
                break;
            default:
                printf("Invalid choice\n");
                exit(EXIT_FAILURE);
        }
    }

    char database[2000];
    if (optind < argc) {
        strncpy(database, argv[optind], sizeof(database) - 1);
    } 
    else {
        printf("Database name not found\n");
        exit(EXIT_FAILURE);
    }

    FILE* file = fopen("users.txt", "r");
    int userFound = 0;
    if (file == NULL) {
        system("touch users.txt");
        system("sudo chmod 777 users.txt");

        FILE* file2 = fopen("users.txt", "r");
        char line[100];
        while (fgets(line, sizeof(line), file2)) {
            line[strcspn(line, "\n")] = '\0';
            if (strstr(line, user) != NULL && strstr(line, password) != NULL) {
                userFound = 1;
                break;
            }
        }
        fclose(file2);
    } 
    else {
        char line[100];
        while (fgets(line, sizeof(line), file)) {
            line[strcspn(line, "\n")] = '\0';
            if (strstr(line, user) != NULL && strstr(line, password) != NULL) {
                userFound = 1;
                break;
            }
        }

        fclose(file);
    }

    if (userFound == 0) {
        printf("User not found\n");
        exit(EXIT_FAILURE);
    } 
    else {
        char cmdUseDB[12002];
        sprintf(cmdUseDB, "\"%s\" USE \"%s\"", user, database);
        if (send(sock, cmdUseDB, strlen(cmdUseDB), 0) < 0) {
            perror("Failed to send user data to server");
            exit(EXIT_FAILURE);
        }

        char full_command[12002];
        sprintf(full_command, "\"%s\" DUMP", user);
        if (send(sock, full_command, strlen(full_command), 0) < 0) {
            perror("Failed to send user data to server");
            exit(EXIT_FAILURE);
        }

        char server_response[2000];
        if (recv(sock, server_response, sizeof(server_response), 0) < 0) {
            perror("Failed to receive a response from the server");
            exit(EXIT_FAILURE);
        }

        char* word = "DROP TABLE";

        while (1) {
            sprintf(full_command, "\"%s\" DUMP", user);
            if (send(sock, full_command, strlen(full_command), 0) < 0) {
                perror("Failed to send user data to server");
                exit(EXIT_FAILURE);
            }

            if (recv(sock, server_response, sizeof(server_response), 0) < 0) {
                perror("Failed to receive a response from the server");
                exit(EXIT_FAILURE);
            }

            printf("%s\n", server_response);

            if (strstr(server_response, word) != NULL) {
                exit(1);
            } else {
                continue;
            }
        }
    }

    //menutup socket
    close(sock);

    return EXIT_SUCCESS;
}
