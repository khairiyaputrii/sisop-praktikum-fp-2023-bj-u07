#define _DEFAULT_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <dirent.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <time.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>

#define SERVER_PORT 8888
#define MAX_COMMAND_LENGTH 2000
char server_message[MAX_COMMAND_LENGTH];

//declare thread karena kita ingin handle multiclient
pthread_t tid[1000];

const char *databasesPath = "/home/fadhlanDocuments/fpcoba/database";

// Function to send data over the socket
int send_all(int sockfd, const char* data, int length) {
    int total_sent = 0;
    int bytes_left = length;
    int bytes_sent;

    while (total_sent < length) {
        bytes_sent = send(sockfd, data + total_sent, bytes_left, 0);
        if (bytes_sent == -1) {
            return -1; // Error occurred
        }
        total_sent += bytes_sent;
        bytes_left -= bytes_sent;
    }

    return 0; // Success
}

//cek permission suatu user dalam database
int cek_permission(const char* database, const char* user){

    FILE* file = fopen("permission.txt", "r");
    if (file == NULL) {
        perror("Gagal membuka file permission.txt");
        exit(EXIT_FAILURE);
    }

    char line[100];
    int aksesFound = 0;
    while (fgets(line, sizeof(line), file)) {
        line[strcspn(line, "\n")] = '\0';
        if (strstr(line, database) != NULL) {
            if (strstr(line, user) != NULL) {
                aksesFound = 1;
                break;
            }
        }
    }

    fclose(file);

    if(aksesFound == 1) return 1;
    else return 0;
}

//cek apakah database telah tersedia. Hal tersebut tercatat dalam permission.txt karena suatu database pasti memiliki setidaknya 1 user yang dapat mengaksesnya
int cek_database(const char* database){

    FILE* file = fopen("permission.txt", "r");
    if (file == NULL) {
        perror("Gagal membuka file permission.txt");
        exit(EXIT_FAILURE);
    }

    char line[100];
    int databaseFound = 0;
    while (fgets(line, sizeof(line), file)) {
        line[strcspn(line, "\n")] = '\0';
        if (strstr(line, database) != NULL) {
            databaseFound = 1;
            break;
        }
    }

    fclose(file);

    if(databaseFound == 1) return 1;
    else return 0;
}

//cek apakah user telah terbuat oleh root. 
int isUserExist(const char* user) {

    FILE* file = fopen("users.txt", "r");
    if (file == NULL) {
        puts("User menjadi user pertama");
        return 0;
    }

    char line[100];
    while (fgets(line, sizeof(line), file) != NULL) {
        char username[50];
        char password[50];
        if (sscanf(line, "%s %s", username, password) == 2) {
            if (strcmp(user, username) == 0) {
                fclose(file);
                return 1; // Pengguna sudah ada
            }
        }
    }

    fclose(file);
    return 0; // Pengguna belum ada
}

//menambahkan hak akses user baru ke suatu database. 
void hak_akses(const char* player, const char* database_name){

    FILE* ptr = fopen("permission.txt","a");
    fprintf(ptr, "%s %s\n", database_name, player);
    fclose(ptr);
}

//menambahkan permission database ke suatu user saat mengirimkan perintah "GRANT PERMISSION"
void give_permission(const char* database, const char* user){

    //cek apakah database telah terbuat
    if(cek_database(database) == 1){
        //cek apakah user tersebut ada
        if(isUserExist(user) == 0){
            puts("Pengguna tidak tersedia");
            return;
        }

        else{
            int hasil = cek_permission(database, user);
            //cek apakah user telah memiliki akses database tersebut atau belum
            if(hasil == 0) hak_akses(user, database);
            else puts("User telah memiliki akses");
        }
    }
    else{
        puts("Database tidak tersedia");
        return;
    }

}

//menyimpan user baru
void saveUser(const char* user, const char* password) {
    if (isUserExist(user)) {
        printf("Pengguna '%s' sudah ada\n", user);
        strcpy(server_message, "User telah tersedia");
        return;
    }
    else strcpy(server_message, "User berhasil disimpan di database");

    FILE* file = fopen("users.txt", "a");
    if (file == NULL) {
        perror("Gagal membuka file users.txt");
        return;
    }

    fprintf(file, "%s %s\n", user, password);
    fclose(file);

    printf("Menyimpan user '%s' dengan password '%s' ke dalam database\n", user, password);
}

//membuat database 
void createDatabase(char* player, char* database_name) {
    // Tambahkan logika untuk membuat database

    //kita lakukan mkdir karena database merupakan suatu folder
    char command[100], command2[500];
    sprintf(command, "mkdir databases/%s", database_name);

    // Menjalankan perintah menggunakan system()
    int status = system(command);

    if (status == 0) {
        printf("Membuat database '%s'\n", database_name);
        strcpy(server_message, "Database berhasil dibuat");
    } 
    
    else {
        printf("Gagal membuat database '%s'\n", database_name);
        strcpy(server_message, "Gagal membuat database");
    }

    //membuat permission dari suatu database baru dengan user yang sedang dijalankan
    hak_akses(player, database_name);

}

//menghilangkan permission dari suatu user saat drop database
void removePermission(const char *filename, const char *databaseName) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        printf("Gagal membuka file: %s\n", filename);
        exit(EXIT_FAILURE);
    }

    //membuat temporary file
    char tempFilename[1000];
    sprintf(tempFilename, "%s.temp", filename);
    FILE *tempFile = fopen(tempFilename, "w");
    if (tempFile == NULL) {
        printf("Gagal membuat file sementara: %s\n", tempFilename);
        fclose(file);
        exit(EXIT_FAILURE);
    }

    char line[1000];
    int found = 0;

    //menyimpan data selain database yang ingin dihapus ke file temporary
    while (fgets(line, sizeof(line), file) != NULL) {
        if (strstr(line, databaseName) != NULL) {
            found = 1;
            continue;
        }
        fputs(line, tempFile);
    }

    fclose(file);
    fclose(tempFile);

    //remove filename
    if (remove(filename) != 0) {
        printf("Gagal menghapus file: %s\n", filename);
        exit(EXIT_FAILURE);
    }

    //rename file temporary ke filename
    if (rename(tempFilename, filename) != 0) {
        printf("Gagal mengganti nama file: %s\n", tempFilename);
        exit(EXIT_FAILURE);
    }

    if (found) {
        printf("Data permission dengan database %s dihapus dari file %s\n", databaseName, filename);
    } else {
        printf("Data permission dengan database %s tidak ditemukan di file %s\n", databaseName, filename);
    }
}

//cek login untuk mengetahui suatu user sedang mengakses database apa. hal tersebut disimpan dalam use_database.txt
void cek_login(const char* user) {
    // Buka file use_database.txt untuk membaca
    FILE* file = fopen("use_database.txt", "r");
    if (file == NULL) {
        printf("Gagal membuka file use_database.txt\n");
        return;
    }

    char line[100];
    char tempFile[] = "temp.txt";

    // Buka file temporary untuk menulis
    FILE* temp = fopen(tempFile, "w");
    if (temp == NULL) {
        printf("Gagal membuat file temporary\n");
        fclose(file);
        return;
    }

    int deleted = 0; // Flag untuk menandakan apakah ada baris yang dihapus

    // Baca baris per baris dari file use_database.txt
    while (fgets(line, sizeof(line), file) != NULL) {
        // Periksa apakah baris mengandung substring <user>
        char* token = strstr(line, user);
        if (token != NULL) {
            // Hapus substring <user> dari baris
            memmove(token, token + strlen(user), strlen(token) - strlen(user) + 1);
            deleted = 1;
        } else {
            // Tulis baris yang tidak berubah ke file temporary
            fputs(line, temp);
        }
    }

    // Tutup file
    fclose(file);
    fclose(temp);

    // Hapus file use_database.txt
    if (remove("use_database.txt") == 0) {
        // Ganti nama file temporary menjadi use_database.txt
        if (rename(tempFile, "use_database.txt") != 0) {
            printf("Gagal mengganti nama file\n");
        }
    } else {
        printf("Gagal menghapus file use_database.txt\n");
    }

    if (deleted) {
        printf("Baris dengan user %s dihapus dari file use_database.txt\n", user);
    } else {
        printf("Tidak ada baris dengan user %s dalam file use_database.txt\n", user);
    }
}

//saat user memanggil perintah USE()
void use(const char* database, const char* user, int client_socket){

    char message[1000];
    if(cek_database(database) == 1){

        if((strcmp(user, "root") == 0) || (cek_permission(database, user) == 1)){
            sprintf(message, "User %s menggunakan database %s\n", user, database);
            printf("User %s menggunakan database %s\n", user, database);
            send_all(client_socket, message, strlen(message));

            //memanggil cek_login() untuk memberitahu bahwa user sedang mengakses suatu database
            cek_login(user);

            FILE* file = fopen("use_database.txt", "a");
            if (file != NULL) {
                // Tulis string "<user> <database>" ke dalam file
                fprintf(file, "%s %s\n", user, database);
                // Tutup file setelah selesai menulis
                fclose(file);
            } 
            
            else {
                sprintf(message, "Gagal membuka file use_database.txt\n");
                printf("Gagal membuka file use_database.txt\n");

                send_all(client_socket, message, strlen(message));
            }
        }
        else{
            sprintf(message, "User %s tidak memiliki akses database %s\n", user, database);
            printf("User %s tidak memiliki akses database %s\n", user, database);

            send_all(client_socket, message, strlen(message));
        } 
    }

    else {
        sprintf(message, "Database tidak ditemukan\n");
        puts("database tidak ditemukan");
        send_all(client_socket, message, strlen(message));
    }
}

//untuk mengetahui user sedang mengakses database apa, diketahui dari perintah use() yang telah dipanggil sebelumnya
void getDatabaseFromFile(const char* user, const char* filename, char* database) {
    FILE* file = fopen(filename, "r");

    if (file == NULL) {
        printf("Failed to open the file\n");
        return;
    }

    char line[500];
    while (fgets(line, sizeof(line), file)) {
        char tmpUser[100], tmpDatabase[100];
        if (sscanf(line, "%s %s", tmpUser, tmpDatabase) == 2) {
            if (strcmp(tmpUser, user) == 0) {
                strcpy(database, tmpDatabase);
                fclose(file);
                return;
            }
        }
    }

    printf("User not found in the file\n");
    fclose(file);
}

//cek apakah tabel telah terbuat
bool isTableExists(const char* path){
    char filePath[500];

    strcpy(filePath, path);

    FILE* file = fopen(filePath, "r");
    if (file) {
        // File ditemukan
        fclose(file);
        return true;
    } else {
        // File tidak ditemukan
        return false;
    }
}

//cek apakah database telah terbuat
bool isDatabaseExists(const char* database_name){

    DIR* dir = opendir("databases");
    if (!dir) {
        // Direktori database tidak ditemukan
        return false;
    }

    struct dirent* entry;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, database_name) == 0) {
            // Direktori dengan nama yang sesuai ditemukan
            closedir(dir);
            return true;
        }
    }

    // Direktori tidak ditemukan
    closedir(dir);
    return false;
}

//menghapus karakter whitespace di awal string text dan mengembalikan pointer ke string yang telah di trim
char* trim(char *text){
    int index = 0;

    while(text[index] == ' ' || text[index] == '\t'){
        index++;
    }
    char *temp = strchr(text,text[index]);
    return temp;
}

//untuk cek apakah buff tersebut merupakan int ataukah string
int validasi(char* buff){
    char tmp[1000];
    strcpy(tmp, buff);

    char g = '\'';

    if(tmp[0] == g && tmp[strlen(tmp) - 1] == g) return 1;

    for(int i = 0; i < strlen(tmp); i++){
        
        //jika bukan merupakan string atau int
        if(tmp[i] < '0' || tmp[i] > '9'){
            return 0;
        }
    }
    return 2;

}

//fungsi yang dijalankan saat user memanggil perintah insert()
void insert(const char* cmnd, const char* database, const char* tableName){

    char tmp[1000];
    strcpy(tmp, cmnd);

    //mengambil value didalam tanda kurung
    char* token = strtok(tmp, "(");
    printf("token1 = %s\n", token);
    token = strtok(NULL, "(");
    printf("token2 = %s\n", token);
    token = strtok(token, ")");
    printf("token3 = %s\n", token);
    token = strtok(token, ",");
    printf("token4 = %s\n", token);

    char data[100][100];

    int i = 0;

    //mengambil kata dalam koma 
    while(token != NULL){
        strcpy(data[i], trim(token));
        i++;
        token = strtok(NULL, ",");
        printf("loop token = %s\n", token);
    }

    FILE *filein, *fileout;

    char open[1000] = {0}, appends[1000] = {0};
    sprintf(open, "databases/%s/struktur_%s.txt", database, tableName);

    printf("open: %s\n", open);
    filein = fopen(open, "r");

    char data_type[100][1000];
    char tmp_type[1000];
    int k = 0, l=0;

    //cek table
    if(filein){
        while(fscanf(filein,"%s",tmp_type) != EOF){
            if (l%2 == 1){
                strcpy(data_type[k],tmp_type);
                printf("Datatype[%d]=%s\n",k,data_type[k]);
                k++;
            }
            l++;
        }
    }
    else{
        puts("Table tidak ada");
        return;
    }

    printf("k = %d, i = %d\n", k, i);

    sprintf(appends, "databases/%s/%s.txt", database, tableName);
    fileout = fopen(appends, "a");

    if(k != i){
        fclose(filein);
        fclose(fileout);

        puts("colom count tidak cocok");
        return;
    }

    //melakukan validasi apakah tipe data sesuai dengan yang diinginkan
    for(int j = 0;j < i; j++){
        int val = validasi(data[j]);
        printf("nilai validasi: %d\n", val);
        if(val == 1 && !strcmp(data_type[j],"string")) ;
        else if (val == 2 && !strcmp(data_type[j],"int")) ;
        else {
            fclose(filein);
            fclose(fileout);
            puts("Invalid input");
            return;
        }  
    }

    for(int j = 0;j< i - 1;j++){

        fprintf(fileout,"%s,", data[j]);
        printf("data: %s\n", data[j]);
    }

    fprintf(fileout,"%s\n", data[i-1]);
    fclose(filein);
    fclose(fileout);
}

void update(char *cmnd, char *database){

    if(!strlen(database)){
        puts("No database used");
        return;
    }

    char temp[1024] = {0};
    strcpy(temp,cmnd);

    char* token = strtok(temp, ";");
    token = strtok(temp," ");
    char input[10][1000];
    int i=0;

    // Tokenisasi kemudian disimpan dalam array input
    while (token != NULL) {
        strcpy(input[i++],token);
        token = strtok(NULL, " ");
    }

    if(strcmp(input[2],"SET")){
        puts("Invalid Syntax");
        return;
    }

    char ColName[1000], val[1000];

    token = strtok(input[3],"=");
    strcpy(ColName,token);
    token = strtok(NULL,"=");
    strcpy(val,token);

    char path[10000],RefFile[10000],n_path[10000];

    sprintf(RefFile,"databases/%s/struktur_%s.txt",database,input[1]);
    FILE *strukturin;
    strukturin = fopen(RefFile,"r");

    i = -1;
    int k = 0;
    char ret[1000],table[1000];

    // Cek Tabel
    if(strukturin){
        while(fscanf(strukturin,"%s %s",table,ret) != EOF){
            if(!strcmp(ColName,table)){
                i = k;
            }
            k++;
        }
        if(i == -1){
            fclose(strukturin);
            puts("Column doesn't exist");
        }
        fclose(strukturin);
    }
    else{
        puts("Table doesn't exist");
    }

    int w = -1;
    char req[1000],temporary[1000],valwhere[1000];

    // Kasus ada klausa where
    if(strlen(input[4])){
        token = strtok(input[5],"=");
        strcpy(req,token);
        token = strtok(NULL,"=");
        strcpy(valwhere,token);
        k=0;
        if(!strcmp(input[4],"WHERE")){
            strukturin = fopen(RefFile,"r");
            while(fscanf(strukturin,"%s %s",temporary,ret) != EOF){
                if(!strcmp(req,temporary)){
                    w = k;
                }
                k++;
            }
            if(w == -1){
                fclose(strukturin);
                puts("Column doesn't exist");
                return;
            }
            fclose(strukturin);
        }
    }

    sprintf(path,"databases/%s/%s.txt",database,input[1]);
    sprintf(n_path,"databases/%s/%s2.txt",database,input[1]);
    FILE *filein,*fileout;
    filein = fopen(path,"r");
    fileout = fopen(n_path,"w");
    char ambil[1000], lama[1000];
    
    // Menulis hasil update
    if(filein){
        
        // Membaca dan mengabaikan update baris pertama (header)
        fprintf(fileout, "%s", fgets(ambil, 1000, filein));
        while(fgets(ambil,1000,filein)){
            strcpy(lama,ambil);
            token = strtok(ambil,",");
            int j = 0;
            char baru[1000];
            strcpy(baru,"");
            int flag = 0;
            while(token!=NULL){
                if(j == w){
                    if(!strcmp(valwhere,token)){
                        flag = 1;
                    }
                }
                if(j==i){
                    strcat(baru,val);
                    strcat(baru,",");
                }
                else{
                    strcat(baru,token);
                    strcat(baru,",");
                }
                token = strtok(NULL,",");
                j++;
            }
            if(flag == 1 || w == -1){
                baru[strlen(baru)-1] = 0;
                fprintf(fileout,"%s",baru);
                if(baru[strlen(baru)-1] != '\n'){
                    fprintf(fileout,"\n");
                }
            }
            else{
                lama[strlen(baru)-1] = 0;
                fprintf(fileout,"%s",lama);
                if(lama[strlen(lama)-1] != '\n'){
                    fprintf(fileout,"\n");
                }
            }
        }
        fclose(filein);
        fclose(fileout);
        remove(path);
        rename(n_path,path);
        return;
    }
}


void delete_from(char *cmnd, char *database) {
    if(!strlen(database)){
        puts("No database used");
        return;
    }

    char temp[1024] = {0};
    strcpy(temp, cmnd);

    // Tokenisasi input untuk mendapat nama tabel
    char* token = strtok(temp, ";");
    token = strtok(temp, " ");
    char input[10][1000];
    int i = 0;

    while (token != NULL) {
        strcpy(input[i++], token);
        token = strtok(NULL, " ");
    }

    // Delete semua baris dari tabel kecuali baris 1 header
    if (i == 3) {
        char path[10000];
        sprintf(path, "databases/%s/%s.txt", database, input[2]);
        FILE *filein, *fileout;
        filein = fopen(path, "r");
        if (filein) {
            char ambil[1000];
            fgets(ambil, 1000, filein);
            fclose(filein);

            fileout = fopen(path, "w");
            fprintf(fileout, "%s\n", ambil);
            fclose(fileout);
        }
        else {
            puts("Table doesn't exist");
            return;
        }
        return;
    }

    // Kasus jika terdapat klausa where
    else if (i == 5) {
        if (strcmp(input[3], "WHERE")) {
            puts("Invalid Syntax");
            return;
        }

        char TableName[1000], val[1000];

        // Tokenisasi untuk mendapat value where
        token = strtok(input[4], "=");
        strcpy(TableName, token);
        token = strtok(NULL, "=");
        strcpy(val, token);

        char path[10000], RefFile[10000], n_path[10000];

        sprintf(RefFile, "databases/%s/struktur_%s.txt", database, input[2]);
        FILE *strukturin;
        strukturin = fopen(RefFile, "r");

        int columnIndex = -1;
        int k = 0;
        char ret[1000], table[1000];
        if (strukturin) {
            while (fscanf(strukturin, "%s %s", table, ret) != EOF) {
                if (!strcmp(TableName, table)) {
                    columnIndex = k;
                }
                k++;
            }
            if (columnIndex == -1) {
                fclose(strukturin);
                puts("Column doesn't exist");
                return;
            }
            fclose(strukturin);
        }
        else {
            puts("Table doesn't exist");
            return;
        }

        sprintf(path, "databases/%s/%s.txt", database, input[2]);
        sprintf(n_path, "databases/%s/%s2.txt", database, input[2]);
        FILE *filein, *fileout;
        filein = fopen(path, "r");
        fileout = fopen(n_path, "w");
        char ambil[1000];
        
        // Mengganti struktur tabel dengan memindahkan file lama yang sesuai ke file baru kemudian direname
        if (filein) {
            int lineCount = 0;
            while (fgets(ambil, 1000, filein)) {
                lineCount++;
                if (lineCount == 1) {
                    fprintf(fileout, "%s", ambil);  // Menyalin baris pertama ke fileout
                    continue;
                }
                token = strtok(ambil, ",");
                int j = 0;
                char baru[1000];
                strcpy(baru, "");
                int flag = 0;
                while (token != NULL) {
                    strcat(baru, token);
                    strcat(baru, ",");
                    if (j == columnIndex) {
                        if (!strcmp(val, token)) {
                            flag = 1;
                        }
                    }
                    token = strtok(NULL, ",");
                    j++;
                }
                if (!flag) {
                    baru[strlen(baru)-1] = 0;
                    fprintf(fileout, "%s", baru);
                    if (baru[strlen(baru)-1] != '\n') {
                        fprintf(fileout, "\n");
                    }
                }
            }
            fclose(filein);
            fclose(fileout);
            remove(path);
            rename(n_path, path);
            return;
        }
    }
}


void select_table(char *cmnd, char *database) {
    if (!strlen(database)) {
        puts("no database used");
        return;
    }

    printf("database: %s\n", database);

    char temp[1024] = {0};
    strcpy(temp, cmnd);

    char* token = strtok(temp, ";");
    token = strtok(temp, " ");
    char input[10][1000];
    int i = 0;

    // Menyimpan potongan command kedalam array input 
    while (token != NULL) {
        strcpy(input[i++], token);
        token = strtok(NULL, " ");
    }

    printf("Input values:\n");
    for (int j = 0; j < i; j++) {
        printf("input[%d]: %s\n", j, input[j]);
    }

    int all, w = -1, k, kasus, urut[20];
    char banding[1000], temporary[1000], cmpwhere[1000], ret[1000];
    char path[10000], struk[10000], n_path[10000], cmp[20][1000];

    FILE *strukturin;

    // Untuk mendapatkan select "*"
    if (!strcmp(input[1], "*")) {
        sprintf(struk, "databases/%s/struktur_%s.txt", database, input[3]);

        strukturin = fopen(struk, "r");

        if (!strukturin) {
            puts("table does not exist");
            return;
        }

        fclose(strukturin);

        all = -1;
        kasus = 2;
        if (strcmp(input[2], "FROM")) {
            puts("invalid syntax");
            return;
        }
        // Cek klausa where
        if (i >= 5 && !strcmp(input[4], "WHERE")) {
            token = strtok(input[5], "=");
            strcpy(banding, token);
            token = strtok(NULL, "=");
            strcpy(cmpwhere, token);

            k = 0;

            if (!strcmp(input[4], "WHERE")) {
                strukturin = fopen(struk, "r");
                while (fscanf(strukturin, "%s %s", temporary, ret) != EOF) {
                    if (!strcmp(banding, temporary)) {
                        w = k;
                    }
                    k++;
                }
                if (w == -1) {
                    fclose(strukturin);
                    puts("column does not exist");
                    return;
                }
                fclose(strukturin);
            }
        }
    }
    // Untuk mendapatkan select beberapa parameter kolom
    else {
        kasus = 2;
        while (kasus <= i && strcmp(input[kasus++], "FROM")) {}

        if (kasus > i) {
            puts("invalid syntax");
            return;
        }
        sprintf(struk, "databases/%s/struktur_%s.txt", database, input[kasus]);
        strukturin = fopen(struk, "r");

        if (!strukturin) {
            puts("table does not exist");
            return;
        }

        fclose(strukturin);

        all = 0;
        kasus = 1;

        for (int oo = 0; oo < 20; oo++) {
            urut[oo] = -1;
        }

        char temp2[1000];
        while (kasus < i && strcmp(input[kasus], "FROM")) {
            k = 0;
            strukturin = fopen(struk, "r");
            while (fscanf(strukturin, "%s %s", temporary, ret) != EOF) {
                strcpy(temp2, temporary);
                strcat(temp2, ",");
                if (!strcmp(input[kasus], temporary) || !strcmp(temp2, input[kasus])) {
                    strcpy(cmp[all], input[kasus]);
                    urut[all] = k;
                    all++;
                }
                k++;
            }
            if (all == 0 || urut[all - 1] == -1) {
                fclose(strukturin);
                return;
            }
            fclose(strukturin);
            kasus++;
        }
        if (kasus == i) {
            return;
        }
        if (i >= kasus + 2 && !strcmp(input[kasus + 2], "WHERE")) {
            token = strtok(input[kasus + 3], "=");
            strcpy(banding, token);
            token = strtok(NULL, "=");
            strcpy(cmpwhere, token);

            k = 0;

            if (!strcmp(input[kasus + 2], "WHERE")) {
                strukturin = fopen(struk, "r");
                while (fscanf(strukturin, "%s %s", temporary, ret) != EOF) {
                    if (!strcmp(banding, temporary)) {
                        w = k;
                    }
                    k++;
                }
                if (w == -1) {
                    fclose(strukturin);
                    return;
                }
                fclose(strukturin);
            }
        }
    }

    sprintf(path, "databases/%s/%s.txt", database, input[kasus + 1]);
    sprintf(n_path, "Hasil_select.txt");

    printf("path: %s\n", path);
    printf("n_path: %s\n", n_path);

    // Attempt to create the output file using touch
    FILE *touch_file = fopen(n_path, "a");
    if (touch_file) {
        fclose(touch_file);
        puts("Output file created.");
    }

    FILE *filein, *fileout;
    filein = fopen(path, "r");
    
    // Menulis hasil select di hasil_select.txt
    if (filein) {

        // Check if output file exists
        fileout = fopen(n_path, "r");
        if (fileout) {
            // Output file already exists
            fclose(fileout);
            puts("Output file already exists.");
        } else {
            // Output file doesn't exist, create it
            fileout = fopen(n_path, "w");
            if (fileout) {
                fclose(fileout);
                puts("Output file created.");
            } else {
                puts("Failed to create output file.");
                return;
            }
        }
        // Open the output file for writing
        fileout = fopen(n_path, "w");
        if (!fileout) {
            puts("Failed to open output file for writing.");
            return;
        }

        char ambil[1000], lama[1000], baru[1000];

        while (fgets(ambil, 1000, filein)) {
            if (all == -1) {
                if (w == -1) {
                    fprintf(fileout, "%s", ambil);
                }
                else {
                    char hade[1000];
                    strcpy(hade, ambil);
                    token = strtok(ambil, ",");
                    int j = 0;
                    strcpy(baru, "");
                    int flag = 0;
                    while (token != NULL) {
                        if (j == w) {
                            if (!strcmp(cmpwhere, token)) {
                                fprintf(fileout, "%s", hade);
                                break;
                            }
                        }
                        token = strtok(NULL, ",");
                        j++;
                    }
                }
            }
            else {
                char jadi[1000], hade[1000];
                strcpy(jadi, "");
                for (int oo = 0; oo < all; oo++) {
                    strcpy(hade, ambil);
                    token = strtok(hade, ",");
                    int j = 0;
                    strcpy(baru, "");
                    int flag = 0;
                    while (token != NULL) {
                        if (j == urut[oo]) {
                            strcat(jadi, token);
                            strcat(jadi, ",");
                            break;
                        }
                        token = strtok(NULL, ",");
                        j++;
                    }
                }
                jadi[strlen(jadi) - 1] = '\0';
                fprintf(fileout, "%s\n", jadi);
            }
        }

        fclose(filein);
        fclose(fileout);
        puts("Select result has been written to the file 'database/Hasil_select.txt'");
    }
    else {
        puts("Failed to open input or output file");
    }
} 


void handleDump(char *db_name, char *dumpOutput) {
    //cek apakah ada database
    if (strlen(db_name) == 0) {
        puts("No database used");
        return;
    }

    //buat file path
    char filePath[100];
    snprintf(filePath, sizeof(filePath), "databases/%s", db_name);

    DIR *dir = opendir(filePath);
    if (dir == NULL) {
        perror("Error opening directory");
        printf("Directory path: %s\n", filePath);
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG && strncmp(entry->d_name, "struktur_", 9) != 0) {
            // Create the file path
            char file[2000];
            snprintf(file, sizeof(file), "%s/%s", filePath, entry->d_name);

            // Open the file
            FILE *filePtr = fopen(file, "r");
            if (filePtr == NULL) {
                printf("Error opening file: %s\n", file);
                continue;
            }

            // Read the first line
            char firstLine[1000];
            if (fgets(firstLine, sizeof(firstLine), filePtr) != NULL) {
                // Remove newline character from the end of the line
                if (firstLine[strlen(firstLine) - 1] == '\n') {
                    firstLine[strlen(firstLine) - 1] = '\0';
                }

                // Remove file extension
                char *extension = strrchr(entry->d_name, '.');
                if (extension != NULL) {
                    *extension = '\0';
                }

                // Append the output to the dumpOutput variable
                sprintf(dumpOutput + strlen(dumpOutput), "DROP TABLE %s;\n", entry->d_name);
                sprintf(dumpOutput + strlen(dumpOutput), "CREATE TABLE %s (%s);\n", entry->d_name, firstLine);

                // Read the rest of the lines
                char line[1000];
                while (fgets(line, sizeof(line), filePtr) != NULL) {
                    // Remove newline character from the end of the line
                    if (line[strlen(line) - 1] == '\n') {
                        line[strlen(line) - 1] = '\0';
                    }

                    // Append the output to the dumpOutput variable
                    sprintf(dumpOutput + strlen(dumpOutput), "INSERT INTO %s (%s);\n", entry->d_name, line);
                }

                sprintf(dumpOutput + strlen(dumpOutput), "\n");
            }

            // Close the file
            fclose(filePtr);
        }
    }

    closedir(dir);
}


//untuk membuat log dari history perintah
void log_command(const char *username, const char *command) {
    //menggunakan time saat ini
    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm_info);

    FILE *file = fopen("log.txt", "a");
    if (file == NULL) {
        perror("Gagal membuka file log.txt");
        return;
    }

    //menghilangkan ";"
    char cleaned_command[100];
    strcpy(cleaned_command, command);
    char *semicolon_pos = strchr(cleaned_command, ';');
    if (semicolon_pos != NULL) {
        *semicolon_pos = '\0';
    }

    //memasukkan ke log.txt
    fprintf(file, "%s:%s:%s\n", timestamp, username, cleaned_command);

    fclose(file);
}

//menjalankan database.c nya saat di daemon
void* start(void *arg){

    int new_socket = *(int *) arg;
    char buffer[10000];

    while (1) {
        // Menerima informasi dari client
        int bytes_received = recv(new_socket, buffer, sizeof(buffer) - 1, 0);
        if (bytes_received < 0) {
            perror("Gagal menerima data dari client");
            exit(EXIT_FAILURE);
        }
        buffer[bytes_received] = '\0'; // Null-terminate the received data

        printf("Command: %s\n", buffer);

        //send keterangan command
        send(new_socket, buffer, strlen(buffer), 0);

        // Mengirim respons ke client
        if (send(new_socket, "Pesan diterima oleh server", strlen("Pesan diterima oleh server"), 0) < 0) {
            perror("Gagal mengirim respons ke client");
            exit(EXIT_FAILURE);
        }

        // Separate the string based on quotation marks
        char *player = strtok(buffer, "\"");

        printf("player: %s\n", player);

        // Get the remaining part of the string after the separation
        char *cmnd = strtok(NULL, ";");

        // Skip leading spaces in the remaining string
        cmnd += strspn(cmnd, " ");

        printf("command: %s\n", cmnd);

        //menuliskan di log
        log_command(player, cmnd);

        //cek perintah
        char detect[10000], argument[10000];
        sscanf(cmnd, "%s %s", detect, argument);

        //jika kata awalnya merupakan CREATE
        if(strcmp(detect, "CREATE") == 0){
            //jika kata selanjutnya merupakan USER
            if(strcmp(argument, "USER") == 0){

                char user[100];
                char password[100];

                //mengambil value user dan password dari terminal
                sscanf(cmnd, "CREATE USER %s IDENTIFIED BY %s", user, password);
                //menjalankan fungsi saveUser()
                saveUser(user, password);
            }
            //jika kata selanjutnya merupakan DATABASE
            else if(strcmp(argument, "DATABASE") == 0){
                char database_name[100];
                //mengambil value database_name
                sscanf(cmnd, "CREATE DATABASE %s", database_name);
                //menjalankan fungsi createDatabase()
                createDatabase(player, database_name);
            }
            //jika kata selanjutnya merupakan TABLE
            else if(strcmp(argument, "TABLE") == 0){
                
                //mengetahui database yang sedang digunakan user() dari use_database.txt
                char database[500];
                getDatabaseFromFile(player, "use_database.txt", database);

                printf("database: %s\n", database);

                char tableName[1000];
                char columns[1000];

                //jika user telah menggunakan database, setelah akses USE()
                if (database[0] != '\0'){
                    
                    //mengambil value tableName dan juga isinya
                    if (sscanf(cmnd, "CREATE TABLE %s (%[^)])", tableName, columns) == 2) {
                        printf("Table name: %s\n", tableName);
                        printf("Columns: %s\n", columns);

                        //kita cek dulu apakah user memiliki akses di suatu database
                        int hasil = cek_permission(database, player);

                        if(hasil == 0) puts("User tidak memiliki akses");
                        else{

                            // Membuat nama file dengan format <tableName>.txt
                            char filename[5000], filename2[5000];
                            char tmp[10000], tmp2[10000];
                            sprintf(filename, "databases/%s/%s.txt", database, tableName);
                            sprintf(filename2, "databases/%s/struktur_%s.txt", database, tableName);
                            
                            //cek apakah tabel telah tersedia
                            if(isTableExists(filename) == 1) puts("Table is already existed");
                            else{
                                //jika belum tersedia maka buat terlebih dahulu dan ubah permission jadi 777
                                sprintf(tmp, "touch %s", filename);
                                sprintf(tmp2, "sudo chmod 777 %s", filename);

                                system(tmp);
                                system(tmp2);

                                sprintf(tmp, "touch %s", filename2);
                                sprintf(tmp2, "sudo chmod 777 %s", filename2);
                                system(tmp);
                                system(tmp2);
                                
                                // Membuka file untuk ditulis
                                FILE *file = fopen(filename, "w");
                                if (file == NULL) {
                                    printf("Failed to create file\n");
                                } 
                                
                                else {
                                    // Menulis isi kolom ke dalam file
                                    fprintf(file, "%s\n", columns);
                                    fclose(file);
                                    printf("Columns have been saved to file %s\n", filename);
                                }

                                FILE *file2 = fopen(filename2, "w");
                                if(file2 == NULL) puts("Failed to create file");
                                else{

                                    char* token;
                                    token = strtok(columns, ",");

                                    int i = 0;

                                    while(token != NULL){

                                            while (*token == ' ') token++;
                                            if (strlen(token) > 0) {
                                                fprintf(file2, "%s\n", token);
                                                printf("%s\n", token);
                                            }
                                            
                                            // Menggunakan strtok lagi untuk memperoleh token berikutnya
                                            token = strtok(NULL, ",");
                                        
                                        i++;
                                    }

                                    fclose(file2);
                                }
                            }

                        } 

                    }
                    
                    else {
                        puts("User belum menggunakan database apapun. Gunakan command USE terlebih dahulu");
                    }
                }

                else puts("User belum mengakses database apapun. Gunakan USE untuk akses suatu database");

            
            }
        }
        //Jika kata awalnya merupakan GRANT
        else if(strcmp(detect, "GRANT") == 0){

            char nama_database[100];
            char user_database[100];
            //mengambil value nama_database dan user_database
            sscanf(cmnd, "GRANT PERMISSION %s INTO %s", nama_database, user_database);
            
            //menjalankan fungsi give_permission() untuk memberikan akses user agar dapat mengakses database yang diinginkan
            give_permission(nama_database, user_database);
        }

        //Jika kata awalnya merupakan USE
        else if(strcmp(detect, "USE") == 0){

            char databaseName[100];

            //mengambil value databaseName
            sscanf(cmnd, "USE %s", databaseName);
            //menjalankan fungsi use() untuk mengetahui bahwa player akan akses suatu database
            use(databaseName, player, new_socket);
        }

        //jika kata awalnya merupakan DROP
        else if(strcmp(detect, "DROP") == 0){
            //Jika kata berikutnya merupakan DATABASE
            if(strcmp(argument, "DATABASE") == 0){
                
                //mengambil value databaseName 
                char databaseName[5000];
                sscanf(cmnd, "DROP DATABASE %s", databaseName);
                //cek permission dari user ke suatu database. jika dia merupakan root maka bebas
                if(cek_permission(databaseName, player) || (strcmp(player, "root") == 0)){
                    //cek apakah database tersedia
                    if(isDatabaseExists(databaseName)){
                        char cmd[10000];

                        //jika tersedia maka hapus database
                        sprintf(cmd, "rm -r databases/%s",databaseName);
                        system(cmd);
                        //menjalankan fungsi removePermission() untuk menghapus user dan database yang tersedia dalam permission.txt
                        removePermission("permission.txt", databaseName);
                    }

                    else puts("Database tidak tersedia");
                }
                else puts("User tidak memiliki akses");

            }
            // Jika kata berikutnya merupakan TABLE
            else if(strcmp(argument, "TABLE") == 0){
                
                //mengambil value tableName
                char tableName[500];
                sscanf(cmnd, "DROP TABLE %s", tableName);

                //mengecek user tersebut sedang mengakses database apa
                char database[500];
                getDatabaseFromFile(player, "use_database.txt", database);

                printf("database: %s\n", database);

                if (database[0] != '\0'){
                    
                    char filename[5000];
                    sprintf(filename, "databases/%s/%s.txt", database, tableName);

                    char filename2[5000];
                    sprintf(filename2, "databases/%s/struktur_%s.txt", database, tableName);

                    //cek apakah table tersedia
                    if(isTableExists(filename) == 1){
                        char cmd_drop[10000];
                        sprintf(cmd_drop, "rm -r %s", filename);
                        system(cmd_drop);

                        sprintf(cmd_drop, "rm -r %s", filename2);
                        system(cmd_drop);
                    }
                    else puts("Table tidak tersedia");

                }
                else puts("User belum mengakses database apapun. Gunakan USE untuk akses suatu database");

            }
            //Jika kata berikutnya merupakan COLUMN
            else if(strcmp(argument, "COLUMN") == 0){
                char tableName[500];
                char colName[500];
                sscanf(cmnd, "DROP COLUMN %s FROM %s;", colName, tableName);

                char database[500];
                getDatabaseFromFile(player, "use_database.txt", database);

                if(!strlen(database)){
                    puts("no database used");
                }

                char input[10][1000];
                char tmp[10000];
                strcpy(tmp,cmnd);
                int k = 0;
                char *token;
                token = strtok(tmp,";");
                token = strtok(token," ");
                while (token != NULL) {
                    strcpy(input[k++],token);
                    token = strtok(NULL, " ");
                }

                if(k != 5){
                    puts("Invalid Syntax");
                }

                FILE *strukturin,*strukturout
                    ,*tablein,*tableout;

                char open[10000]={0},append[10000]={0},a[1000]={0},b[1000]={0};
                sprintf(open,"databases/%s/struktur_%s",database,input[4]);
                strcpy(a,open);
                strcat(a,"2.txt");
                strcat(open,".txt");

                strukturin = fopen(open,"r");
            
                int i = 0,j = 0;
                char data_type[1000], name[1000];

                if(strukturin){
                    strukturout = fopen(a,"w");
                    while(fscanf(strukturin,"%s %s",name,data_type) != EOF){
                        if(strcmp(input[2],name)){
                            fprintf(strukturout,"%s %s\n",name,data_type);
                        }
                        else i = j;
                        j++;
                    }
                    fclose(strukturin);
                    fclose(strukturout);
                }
                else{
                    puts("Table doesn't exist");
                }

                sprintf(append,"databases/%s/%s",database,input[4]);
                strcpy(b,append);
                strcat(b,"2.txt");
                strcat(append,".txt");

                tablein = fopen(append,"r");
                tableout = fopen(b,"w");

                char ambil[1000];
                while(fgets(ambil,1000,tablein)){
                    token = strtok(ambil,",");
                    int j = 0;
                    char baru[1000];
                    strcpy(baru,"");
                    while(token!=NULL){
                        if(j!=i){
                            strcat(baru,token);
                            strcat(baru,",");
                        }
                        token = strtok(NULL,",");
                        j++;
                    }
                    baru[strlen(baru)-1] = 0;
                    fprintf(tableout,"%s",baru);
                    if(baru[strlen(baru)-1] != '\n'){
                        fprintf(tableout,"\n");
                    }
                }
                fclose(tablein);
                fclose(tableout);
                remove(open);
                rename(a,open);
                remove(append);
                rename(b,append);
            }
        }
        //jika kata awalnya merupakan INSERT
        else if(strcmp(detect, "INSERT") == 0){

            //cek user tersebut sedang akses database apa
            char database[500];
            getDatabaseFromFile(player, "use_database.txt", database);

            printf("database: %s\n", database);

            char tableName[500];
            char val[500];
            if (database[0] != '\0'){
                
                //mengambil value tableName dan isi column
                sscanf(cmnd, "INSERT INTO %s (%[^)])", tableName, val); 
                char filename[10000];
                sprintf(filename, "databases/%s/%s.txt", database, tableName);

                //cek apakah table tersedia
                if(isTableExists(filename) == 1){
                    //menjalankan fungsi insert()
                    insert(cmnd, database, tableName);
                }
                else puts("Table tidak tersedia");
            }

            else puts("User belum mengakses database apapun. Gunakan USE untuk akses suatu database");
        }

        else if(strcmp(detect, "UPDATE") == 0){

            char database[500];
            getDatabaseFromFile(player, "use_database.txt", database);

            printf("database : %s\n", database);

            char tableName[500];
            char columnName[500];
            char val[500];
            if (database[0] != '\0'){

                sscanf(cmnd, "UPDATE %s SET %s=%s;", tableName, columnName, val);
                char filename[10000];
                sprintf(filename, "databases/%s/%s.txt", database, tableName);

                if(isTableExists(filename) == 1){

                    update(cmnd, database);
                }
                else puts("Table tidak tersedia");
            }

            else puts("User belum mengakses database apapun. Gunakan USE untuk akses suatu database");
        }

        else if(strcmp(detect, "DELETE") == 0){

            char database[500];
            getDatabaseFromFile(player, "use_database.txt", database);

            printf("database : %s\n", database);

            char tableName[500];
            if (database[0] != '\0'){

                sscanf(cmnd, "DELETE FROM %s;", tableName);
                char filename[10000];
                sprintf(filename, "databases/%s/%s.txt", database, tableName);

                if(isTableExists(filename) == 1){

                    delete_from(cmnd, database);
                }
                else puts("Table tidak tersedia");
            }

            else puts("User belum mengakses database apapun. Gunakan USE untuk akses suatu database");
        }

        else if(strcmp(detect, "SELECT") == 0){

            char database[500];
            getDatabaseFromFile(player, "use_database.txt", database);

            printf("database: %s\n", database);

            char tableName[500];
            char col[500];
            if (database[0] != '\0'){

                sscanf(cmnd, "SELECT %[^FROM] FROM %s", col, tableName); 

                char filename[10000];
                sprintf(filename, "databases/%s/%s.txt", database, tableName);

                if(isTableExists(filename) == 1){
                    
                    select_table(cmnd, database);
                }
                else puts("Table tidak tersedia");
            }

            else puts("User belum mengakses database apapun. Gunakan USE untuk akses suatu database");
        }

        //apabila exit maka hapus riwayat di cek_login()
        else if(strcmp(detect, "EXIT") == 0){

            cek_login(player);

            return 0;
        }

        //jika perintahnya DUMP maka akan dipanggil fungsi handleDump dengan mengambil nama database dari getDatabaseFromFile()
        else if (strcmp (detect, "DUMP") == 0) {
            char dumpOutput[5000];
            char database[500];
            getDatabaseFromFile(player, "use_database.txt", database);
            handleDump(database, dumpOutput);
            // Mengirim respons ke client
            if (send(new_socket, dumpOutput, strlen(dumpOutput), 0) < 0) {
                perror("Gagal mengirim respons ke client");
                exit(EXIT_FAILURE);
            }

        }
    }
}

//menjalankan daemon
void create_daemon(){

    pid_t pid, sid;        // Variabel untuk menyimpan PID

    pid = fork();     // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
    * (nilai variabel pid < 0) */
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
    * (nilai variabel pid adalah PID dari child process) */
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/home/fadhlan/Documents/fpcoba")) < 0) {
        exit(EXIT_FAILURE);
    }

    // close(STDIN_FILENO);
    // close(STDOUT_FILENO);
    // close(STDERR_FILENO);

    // open("/dev/null", O_RDONLY);
	// open("/dev/null", O_RDWR);
	// open("/dev/null", O_RDWR);
}

int main() {

    // Rest of your code for the main function (including the while loop)
    char use_database[100] = {0};
    //membuat folder databases
    //membuat file permission.txt
    //membuat file use_database.txt
    system("mkdir -p databases");
    system("touch permission.txt");
    system("sudo chmod 777 permission.txt");

    system("touch use_database.txt");
    system("sudo chmod 777 use_database.txt");

    //membuat socket dengan port 8888
    int server_fd, new_socket[1000], valread;
    struct sockaddr_in address;
    int addrlen = sizeof(address);

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Gagal membuat socket");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(SERVER_PORT);

    if (bind(server_fd, (struct sockaddr*)&address, sizeof(address)) < 0) {
        perror("Gagal melakukan binding");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("Gagal mendengarkan koneksi");
        exit(EXIT_FAILURE);
    }

    //memanggil daemon
    create_daemon();

    //mengetahui berapa client yang sedang akses database secara bersamaan
    int jumlah_client = 0;
    char message[1000];
    while (1) {

        //menerima koneksi server
        if ((new_socket[jumlah_client] = accept(server_fd, (struct sockaddr*)&address, (socklen_t*)&addrlen)) < 0) {
            perror("Gagal menerima koneksi");
            exit(EXIT_FAILURE);
        }

        //membuat thread untuk handle banyaknya client
        pthread_create(&(tid[jumlah_client]), NULL, start, &new_socket[jumlah_client]);

        jumlah_client++;

        printf("Client ke-%d terhubung\n", jumlah_client);
    }

    return 0;
}
