#!/bin/bash

backup="/home/ubuntu/Documents/fpsisop/database"
runtime="/home/ubuntu/Documents/fpsisop"
timestamp=$(date +\%Y-\%m-\%d_\%H:\%M:\%S)
zip_file="$timestamp.zip"

# Make a backkup file for every child directory
for dir in "$backup"/*; do
  cd /home/ubuntu/Documents/fpsiso/ && \
  ./dump -u taylor -p swift "$(basename "$dir")" > "$backup_dir/$(basename "$dir").backup"
done

# Copy log.txt file to the backup directory
cp "$runtime/log.txt" "$backup_dir"

# Emptying out the real log.txt file
echo -n > "$runtime/log.txt"

# Make zip files from every backup file and log.txt with the timestamp format
cd "$backup" && zip "$zip_file" *.backup log.txt

# Deleting files with the same name of files inside zip
find "$backup" -maxdepth 1 -type f ! -name "$zip_file" -delete

#0 * * * * /home/ubuntu/Documents/fpsiso/cron_db.sh

